#include "CommLib.h"

CommLib_callbacks_t *pCallbacks = NULL;
CommLib_errorCode_e CommLib_setCallbacks(CommLib_callbacks_t *_pCallbacks) {

    pCallbacks = _pCallbacks;
    if (NULL == pCallbacks) {
        return CommLib_errorCode_missingCallback;
    }
    if (NULL == pCallbacks->pwmChangeColorCallback) {
        return CommLib_errorCode_missingCallback;
    }
    if (NULL == pCallbacks->pwmTurnOnCallback) {
        return CommLib_errorCode_missingCallback;
    }
    if (NULL == pCallbacks->pwmTurnOffCallback) {
        return CommLib_errorCode_missingCallback;
    }
    if (NULL == pCallbacks->updateAlexaCallback) {
        return CommLib_errorCode_missingCallback;
    }
    if (NULL == pCallbacks->updateHomekitCallback) {
        return CommLib_errorCode_missingCallback;
    }
#if (NETWORK_DALI_SUPPORT == ENABLED)
    if (NULL == pCallbacks->daliWriteCallback) {
        return CommLib_errorCode_missingCallback;
    }
#endif //NETWORK_DALI_SUPPORT == ENABLED

    return CommLib_errorCode_success;
}

CommLib_pwmValue_t currentPWMValue = DEFAULT_PWM_VALUE;
void CommLib_setCurrentPWMValue(CommLib_pwmValue_t pwmValue) {
    currentPWMValue = pwmValue;
}
CommLib_pwmValue_t CommLib_getCurrentPWMValue(void) {
    return currentPWMValue;
}

bool currentLightIsOn = false;
void CommLib_setLightStatus(bool lightIsOn) {
    currentLightIsOn = lightIsOn;
}
bool CommLib_getLightStatus(void) {
    return currentLightIsOn;
}

CommLib_errorCode_e CommLib_executePWMChangeColorCallback(CommLib_pwmValue_t pwmValue) {
    if (NULL == pCallbacks->pwmChangeColorCallback) {
        return CommLib_errorCode_missingCallback;
    }
    pCallbacks->pwmChangeColorCallback(pwmValue);
    return CommLib_errorCode_success;
}
CommLib_errorCode_e CommLib_executePWMTurnOnCallback(void) {
    if (NULL == pCallbacks->pwmTurnOnCallback) {
        return CommLib_errorCode_missingCallback;
    }
    pCallbacks->pwmTurnOnCallback();
    return CommLib_errorCode_success;
}
CommLib_errorCode_e CommLib_executePWMTurnOffCallback(void) {
    if (NULL == pCallbacks->pwmTurnOffCallback) {
        return CommLib_errorCode_missingCallback;
    }
    pCallbacks->pwmTurnOffCallback();
    return CommLib_errorCode_success;
}

CommLib_errorCode_e CommLib_executeUpdateAlexaCallback(CommLib_AlexaLightValues_t lightValue) {
    if (NULL == pCallbacks->updateAlexaCallback) {
        return CommLib_errorCode_missingCallback;
    }
    pCallbacks->updateAlexaCallback(lightValue);
    return CommLib_errorCode_success;
}

CommLib_errorCode_e CommLib_executeUpdateHomekitCallback(CommLib_HomekitLightValues_t lightValue) {
    if (NULL == pCallbacks->updateHomekitCallback) {
        return CommLib_errorCode_missingCallback;
    }
    pCallbacks->updateHomekitCallback(lightValue);
    return CommLib_errorCode_success;
}

#if (NETWORK_DALI_SUPPORT == ENABLED)
CommLib_errorCode_e CommLib_executeDaliWriteCallback(uint8_t backwardFrame) {
    if (NULL == pCallbacks->daliWriteCallback) {
        return CommLib_errorCode_missingCallback;
    }
    pCallbacks->daliWriteCallback(backwardFrame);
    return CommLib_errorCode_success;
}
#endif //NETWORK_DALI_SUPPORT == ENABLED

