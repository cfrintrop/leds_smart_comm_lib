#include "DaliCommands.h"
#include <stdbool.h>

#if (NETWORK_DALI_SUPPORT == ENABLED)

//Internal Functions
void DaliCommands_onCommand(uint8_t command);
void DaliCommands_onDirectCommand(uint8_t command);
bool DaliCommands_isTalkingToMe(uint8_t address);
void DaliCommands_controlLight(uint8_t targetLevel, bool fadeEnabled);

//Internal Variable
static uint8_t DaliCommands_lastCommand = 0x00;
static bool DaliCommands_commandWasRepeated = false;

//Dali Commands
void DaliCommands_dapc(uint8_t level);

void DaliCommands_off(void);
void DaliCommands_up(void);
void DaliCommands_down(void);
void DaliCommands_stepUp(void);
void DaliCommands_stepDown(void);
void DaliCommands_recallMaxLevel(void);
void DaliCommands_recallMinLevel(void);
void DaliCommands_stepDownAndOff(void);
void DaliCommands_onAndStepUp(void);
void DaliCommands_enableDAPCSequence(void);
void DaliCommands_goToLastActiveLevel(void);
void DaliCommands_continuousUp(void);
void DaliCommands_continuousDown(void);
void DaliCommands_goToScene(uint8_t sceneNumber);

void DaliCommands_reset(void);
void DaliCommands_storeActualLevelInDTR0(void);
void DaliCommands_savePersistentVariables(void);
void DaliCommands_setOperatingMode(void);
void DaliCommands_resetMemoryBank(void);
void DaliCommands_identifyDevice(void);
void DaliCommands_setMaxLevel(void);
void DaliCommands_setMinLevel(void);
void DaliCommands_setSystemFailureLevel(void);
void DaliCommands_setPowerOnLevel(void);
void DaliCommands_setFadeTime(void);
void DaliCommands_setFadeRate(void);
void DaliCommands_setExtendedFadeTime(void);

void DaliCommands_setScene(uint8_t sceneX);

void DaliCommands_removeFromScene(uint8_t sceneX);

void DaliCommands_addToGroup(uint8_t group);

void DaliCommands_removeFromGroup(uint8_t group);

void DaliCommands_setShortAddress(void);
void DaliCommands_enableWriteMemory(void);
void DaliCommands_queryStatus(void);
void DaliCommands_queryControlGearPresent(void);
void DaliCommands_queryLampFailure(void);
void DaliCommands_queryLampPowerOn(void);
void DaliCommands_queryLimitError(void);
void DaliCommands_queryResetState(void);
void DaliCommands_queryMissingShortAddress(void);
void DaliCommands_queryVersionNumber(void);
void DaliCommands_queryContentDTR0(void);
void DaliCommands_queryDeviceType(void);
void DaliCommands_queryPhysicalMinimum(void);
void DaliCommands_queryPowerFailure(void);
void DaliCommands_queryContentDTR1(void);
void DaliCommands_queryContentDTR2(void);
void DaliCommands_queryOperatingMode(void);
void DaliCommands_queryLightSourceType(void);

void DaliCommands_queryActualLevel(void);
void DaliCommands_queryMaxLevel(void);
void DaliCommands_queryMinLevel(void);
void DaliCommands_queryPowerOnLevel(void);
void DaliCommands_querySystemFailureLevel(void);
void DaliCommands_queryFadeTimeFadeRate(void);
void DaliCommands_queryManufacturerSpecificMode(void);
void DaliCommands_queryNextDeviceType(void);
void DaliCommands_queryExtendedFadeTime(void);
void DaliCommands_queryControlGearFailure(void);

void DaliCommands_querySceneLevel(uint8_t sceneNumber);

void DaliCommands_queryGroups0To7(void);
void DaliCommands_queryGroups8To15(void);
void DaliCommands_queryRandomAddressH(void);
void DaliCommands_queryRandomAddressM(void);
void DaliCommands_queryRandomAddressL(void);
void DaliCommands_readMemoryLocation(void);
void DaliCommands_applicationExtendedCommands(void);
void DaliCommands_queryExtendedVersionNumber(void);

void DaliCommands_reservedFunction(void);


//TODO Dali
void DaliCommands_SpecialCommands_terminate(void);
void DaliCommands_SpecialCommands_DTR0(void);
void DaliCommands_SpecialCommands_initialise(void);
void DaliCommands_SpecialCommands_randomise(void);
void DaliCommands_SpecialCommands_compare(void);
void DaliCommands_SpecialCommands_withdraw(void);
void DaliCommands_SpecialCommands_ping(void);
void DaliCommands_SpecialCommands_searchAddrH(void);
void DaliCommands_SpecialCommands_searchAddrM(void);
void DaliCommands_SpecialCommands_searchAddrL(void);
void DaliCommands_SpecialCommands_programShortAddress(void);
void DaliCommands_SpecialCommands_verifyShortAddress(void);
void DaliCommands_SpecialCommands_enableDeviceType(void);
void DaliCommands_SpecialCommands_DTR1(void);
void DaliCommands_SpecialCommands_DTR2(void);
void DaliCommands_SpecialCommands_writeMemoryLocation(void);
void DaliCommands_SpecialCommands_writeMemoryLocationNoReply(void);

void DaliCommands_reservedSpecialFunction(uint8_t cmd, uint8_t data_val);



//DaliCommands as Array of Function Pointers
DaliCommands_CommandPtr_t DaliCommands_commandsArr[] = {
  (DaliCommands_CommandPtr_t) DaliCommands_off,                                             //Command 0x00
  (DaliCommands_CommandPtr_t) DaliCommands_up,                                              //Command 0x01
  (DaliCommands_CommandPtr_t) DaliCommands_down,                                            //Command 0x02
  (DaliCommands_CommandPtr_t) DaliCommands_stepUp,                                          //Command 0x03
  (DaliCommands_CommandPtr_t) DaliCommands_stepDown,                                        //Command 0x04
  (DaliCommands_CommandPtr_t) DaliCommands_recallMaxLevel,                                  //Command 0x05
  (DaliCommands_CommandPtr_t) DaliCommands_recallMinLevel,                                  //Command 0x06
  (DaliCommands_CommandPtr_t) DaliCommands_stepDownAndOff,                                  //Command 0x07
  (DaliCommands_CommandPtr_t) DaliCommands_onAndStepUp,                                     //Command 0x08
  (DaliCommands_CommandPtr_t) DaliCommands_enableDAPCSequence,                              //Command 0x09
  (DaliCommands_CommandPtr_t) DaliCommands_goToLastActiveLevel,                             //Command 0x0A
  (DaliCommands_CommandPtr_t) DaliCommands_continuousUp,                                    //Command 0x0B
  (DaliCommands_CommandPtr_t) DaliCommands_continuousDown,                                  //Command 0x0C
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x0D
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x0E
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x0F

  (DaliCommands_CommandPtr_t) DaliCommands_goToScene,                                       //Command 0x10
  (DaliCommands_CommandPtr_t) DaliCommands_goToScene,                                       //Command 0x11
  (DaliCommands_CommandPtr_t) DaliCommands_goToScene,                                       //Command 0x12
  (DaliCommands_CommandPtr_t) DaliCommands_goToScene,                                       //Command 0x13
  (DaliCommands_CommandPtr_t) DaliCommands_goToScene,                                       //Command 0x14
  (DaliCommands_CommandPtr_t) DaliCommands_goToScene,                                       //Command 0x15
  (DaliCommands_CommandPtr_t) DaliCommands_goToScene,                                       //Command 0x16
  (DaliCommands_CommandPtr_t) DaliCommands_goToScene,                                       //Command 0x17
  (DaliCommands_CommandPtr_t) DaliCommands_goToScene,                                       //Command 0x18
  (DaliCommands_CommandPtr_t) DaliCommands_goToScene,                                       //Command 0x19
  (DaliCommands_CommandPtr_t) DaliCommands_goToScene,                                       //Command 0x1A
  (DaliCommands_CommandPtr_t) DaliCommands_goToScene,                                       //Command 0x1B
  (DaliCommands_CommandPtr_t) DaliCommands_goToScene,                                       //Command 0x1C
  (DaliCommands_CommandPtr_t) DaliCommands_goToScene,                                       //Command 0x1D
  (DaliCommands_CommandPtr_t) DaliCommands_goToScene,                                       //Command 0x1E
  (DaliCommands_CommandPtr_t) DaliCommands_goToScene,                                       //Command 0x1F

  /* Configuration Commands */
  (DaliCommands_CommandPtr_t) DaliCommands_reset,                                           //Command 0x20
  (DaliCommands_CommandPtr_t) DaliCommands_storeActualLevelInDTR0,                          //Command 0x21
  (DaliCommands_CommandPtr_t) DaliCommands_savePersistentVariables,                         //Command 0x22
  (DaliCommands_CommandPtr_t) DaliCommands_setOperatingMode,                                //Command 0x23
  (DaliCommands_CommandPtr_t) DaliCommands_resetMemoryBank,                                 //Command 0x24
  (DaliCommands_CommandPtr_t) DaliCommands_identifyDevice,                                  //Command 0x25
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x26
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x27
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x28
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x29
  (DaliCommands_CommandPtr_t) DaliCommands_setMaxLevel,                                     //Command 0x2A
  (DaliCommands_CommandPtr_t) DaliCommands_setMinLevel,                                     //Command 0x2B
  (DaliCommands_CommandPtr_t) DaliCommands_setSystemFailureLevel,                           //Command 0x2C
  (DaliCommands_CommandPtr_t) DaliCommands_setPowerOnLevel,                                 //Command 0x2D
  (DaliCommands_CommandPtr_t) DaliCommands_setFadeTime,                                     //Command 0x2E
  (DaliCommands_CommandPtr_t) DaliCommands_setFadeRate,                                     //Command 0x2F

  (DaliCommands_CommandPtr_t) DaliCommands_setExtendedFadeTime,                             //Command 0x30
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x31
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x32
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x33
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x34
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x35
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x36
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x37
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x38
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x39
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x3A
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x3B
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x3C
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x3D
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x3E
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x3F

  (DaliCommands_CommandPtr_t) DaliCommands_setScene,                                        //Command 0x40
  (DaliCommands_CommandPtr_t) DaliCommands_setScene,                                        //Command 0x41
  (DaliCommands_CommandPtr_t) DaliCommands_setScene,                                        //Command 0x42
  (DaliCommands_CommandPtr_t) DaliCommands_setScene,                                        //Command 0x43
  (DaliCommands_CommandPtr_t) DaliCommands_setScene,                                        //Command 0x44
  (DaliCommands_CommandPtr_t) DaliCommands_setScene,                                        //Command 0x45
  (DaliCommands_CommandPtr_t) DaliCommands_setScene,                                        //Command 0x46
  (DaliCommands_CommandPtr_t) DaliCommands_setScene,                                        //Command 0x47
  (DaliCommands_CommandPtr_t) DaliCommands_setScene,                                        //Command 0x48
  (DaliCommands_CommandPtr_t) DaliCommands_setScene,                                        //Command 0x49
  (DaliCommands_CommandPtr_t) DaliCommands_setScene,                                        //Command 0x4A
  (DaliCommands_CommandPtr_t) DaliCommands_setScene,                                        //Command 0x4B
  (DaliCommands_CommandPtr_t) DaliCommands_setScene,                                        //Command 0x4C
  (DaliCommands_CommandPtr_t) DaliCommands_setScene,                                        //Command 0x4D
  (DaliCommands_CommandPtr_t) DaliCommands_setScene,                                        //Command 0x4E
  (DaliCommands_CommandPtr_t) DaliCommands_setScene,                                        //Command 0x4F

  (DaliCommands_CommandPtr_t) DaliCommands_removeFromScene,                                 //Command 0x50
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromScene,                                 //Command 0x51
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromScene,                                 //Command 0x52
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromScene,                                 //Command 0x53
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromScene,                                 //Command 0x54
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromScene,                                 //Command 0x55
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromScene,                                 //Command 0x56
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromScene,                                 //Command 0x57
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromScene,                                 //Command 0x58
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromScene,                                 //Command 0x59
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromScene,                                 //Command 0x5A
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromScene,                                 //Command 0x5B
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromScene,                                 //Command 0x5C
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromScene,                                 //Command 0x5D
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromScene,                                 //Command 0x5E
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromScene,                                 //Command 0x5F

  (DaliCommands_CommandPtr_t) DaliCommands_addToGroup,                                      //Command 0x60
  (DaliCommands_CommandPtr_t) DaliCommands_addToGroup,                                      //Command 0x61
  (DaliCommands_CommandPtr_t) DaliCommands_addToGroup,                                      //Command 0x62
  (DaliCommands_CommandPtr_t) DaliCommands_addToGroup,                                      //Command 0x63
  (DaliCommands_CommandPtr_t) DaliCommands_addToGroup,                                      //Command 0x64
  (DaliCommands_CommandPtr_t) DaliCommands_addToGroup,                                      //Command 0x65
  (DaliCommands_CommandPtr_t) DaliCommands_addToGroup,                                      //Command 0x66
  (DaliCommands_CommandPtr_t) DaliCommands_addToGroup,                                      //Command 0x67
  (DaliCommands_CommandPtr_t) DaliCommands_addToGroup,                                      //Command 0x68
  (DaliCommands_CommandPtr_t) DaliCommands_addToGroup,                                      //Command 0x69
  (DaliCommands_CommandPtr_t) DaliCommands_addToGroup,                                      //Command 0x6A
  (DaliCommands_CommandPtr_t) DaliCommands_addToGroup,                                      //Command 0x6B
  (DaliCommands_CommandPtr_t) DaliCommands_addToGroup,                                      //Command 0x6C
  (DaliCommands_CommandPtr_t) DaliCommands_addToGroup,                                      //Command 0x6D
  (DaliCommands_CommandPtr_t) DaliCommands_addToGroup,                                      //Command 0x6E
  (DaliCommands_CommandPtr_t) DaliCommands_addToGroup,                                      //Command 0x6F

  (DaliCommands_CommandPtr_t) DaliCommands_removeFromGroup,                                 //Command 0x70
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromGroup,                                 //Command 0x71
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromGroup,                                 //Command 0x72
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromGroup,                                 //Command 0x73
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromGroup,                                 //Command 0x74
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromGroup,                                 //Command 0x75
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromGroup,                                 //Command 0x76
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromGroup,                                 //Command 0x77
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromGroup,                                 //Command 0x78
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromGroup,                                 //Command 0x79
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromGroup,                                 //Command 0x7A
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromGroup,                                 //Command 0x7B
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromGroup,                                 //Command 0x7C
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromGroup,                                 //Command 0x7D
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromGroup,                                 //Command 0x7E
  (DaliCommands_CommandPtr_t) DaliCommands_removeFromGroup,                                 //Command 0x7F

  (DaliCommands_CommandPtr_t) DaliCommands_setShortAddress,                                 //Command 0x80
  (DaliCommands_CommandPtr_t) DaliCommands_enableWriteMemory,                               //Command 0x81
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x82
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x83
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x84
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x85
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x86
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x87
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x88
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x89
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x8A
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x8B
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x8C
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x8D
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x8E
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0x8F

  /* Query Commands */
  (DaliCommands_CommandPtr_t) DaliCommands_queryStatus,                                     //Command 0x90
  (DaliCommands_CommandPtr_t) DaliCommands_queryControlGearPresent,                         //Command 0x91
  (DaliCommands_CommandPtr_t) DaliCommands_queryLampFailure,                                //Command 0x92
  (DaliCommands_CommandPtr_t) DaliCommands_queryLampPowerOn,                                //Command 0x93
  (DaliCommands_CommandPtr_t) DaliCommands_queryLimitError,                                 //Command 0x94
  (DaliCommands_CommandPtr_t) DaliCommands_queryResetState,                                 //Command 0x95
  (DaliCommands_CommandPtr_t) DaliCommands_queryMissingShortAddress,                        //Command 0x96
  (DaliCommands_CommandPtr_t) DaliCommands_queryVersionNumber,                              //Command 0x97
  (DaliCommands_CommandPtr_t) DaliCommands_queryContentDTR0,                                //Command 0x98
  (DaliCommands_CommandPtr_t) DaliCommands_queryDeviceType,                                 //Command 0x99
  (DaliCommands_CommandPtr_t) DaliCommands_queryPhysicalMinimum,                            //Command 0x9A
  (DaliCommands_CommandPtr_t) DaliCommands_queryPowerFailure,                               //Command 0x9B
  (DaliCommands_CommandPtr_t) DaliCommands_queryContentDTR1,                                //Command 0x9C
  (DaliCommands_CommandPtr_t) DaliCommands_queryContentDTR2,                                //Command 0x9D
  (DaliCommands_CommandPtr_t) DaliCommands_queryOperatingMode,                              //Command 0x9E
  (DaliCommands_CommandPtr_t) DaliCommands_queryLightSourceType,                            //Command 0x9F

  (DaliCommands_CommandPtr_t) DaliCommands_queryActualLevel,                                //Command 0xA0
  (DaliCommands_CommandPtr_t) DaliCommands_queryMaxLevel,                                   //Command 0xA1
  (DaliCommands_CommandPtr_t) DaliCommands_queryMinLevel,                                   //Command 0xA2
  (DaliCommands_CommandPtr_t) DaliCommands_queryPowerOnLevel,                               //Command 0xA3
  (DaliCommands_CommandPtr_t) DaliCommands_querySystemFailureLevel,                         //Command 0xA4
  (DaliCommands_CommandPtr_t) DaliCommands_queryFadeTimeFadeRate,                           //Command 0xA5
  (DaliCommands_CommandPtr_t) DaliCommands_queryManufacturerSpecificMode,                   //Command 0xA6
  (DaliCommands_CommandPtr_t) DaliCommands_queryNextDeviceType,                             //Command 0xA7
  (DaliCommands_CommandPtr_t) DaliCommands_queryExtendedFadeTime,                           //Command 0xA8
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xA9
  (DaliCommands_CommandPtr_t) DaliCommands_queryControlGearFailure,                         //Command 0xAA
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xAB
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xAC
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xAD
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xAE
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xAF

  (DaliCommands_CommandPtr_t) DaliCommands_querySceneLevel,                                 //Command 0xB0
  (DaliCommands_CommandPtr_t) DaliCommands_querySceneLevel,                                 //Command 0xB1
  (DaliCommands_CommandPtr_t) DaliCommands_querySceneLevel,                                 //Command 0xB2
  (DaliCommands_CommandPtr_t) DaliCommands_querySceneLevel,                                 //Command 0xB3
  (DaliCommands_CommandPtr_t) DaliCommands_querySceneLevel,                                 //Command 0xB4
  (DaliCommands_CommandPtr_t) DaliCommands_querySceneLevel,                                 //Command 0xB5
  (DaliCommands_CommandPtr_t) DaliCommands_querySceneLevel,                                 //Command 0xB6
  (DaliCommands_CommandPtr_t) DaliCommands_querySceneLevel,                                 //Command 0xB7
  (DaliCommands_CommandPtr_t) DaliCommands_querySceneLevel,                                 //Command 0xB8
  (DaliCommands_CommandPtr_t) DaliCommands_querySceneLevel,                                 //Command 0xB9
  (DaliCommands_CommandPtr_t) DaliCommands_querySceneLevel,                                 //Command 0xBA
  (DaliCommands_CommandPtr_t) DaliCommands_querySceneLevel,                                 //Command 0xBB
  (DaliCommands_CommandPtr_t) DaliCommands_querySceneLevel,                                 //Command 0xBC
  (DaliCommands_CommandPtr_t) DaliCommands_querySceneLevel,                                 //Command 0xBD
  (DaliCommands_CommandPtr_t) DaliCommands_querySceneLevel,                                 //Command 0xBE
  (DaliCommands_CommandPtr_t) DaliCommands_querySceneLevel,                                 //Command 0xBF

  (DaliCommands_CommandPtr_t) DaliCommands_queryGroups0To7,                                 //Command 0xC0
  (DaliCommands_CommandPtr_t) DaliCommands_queryGroups8To15,                                //Command 0xC1
  (DaliCommands_CommandPtr_t) DaliCommands_queryRandomAddressH,                             //Command 0xC2
  (DaliCommands_CommandPtr_t) DaliCommands_queryRandomAddressM,                             //Command 0xC3
  (DaliCommands_CommandPtr_t) DaliCommands_queryRandomAddressL,                             //Command 0xC4
  (DaliCommands_CommandPtr_t) DaliCommands_readMemoryLocation,                              //Command 0xC5
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xC6
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xC7
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xC8
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xC9
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xCA
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xCB
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xCC
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xCD
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xCE
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xCF

  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xD0
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xD1
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xD2
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xD3
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xD4
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xD5
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xD6
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xD7
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xD8
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xD9
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xDA
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xDB
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xDC
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xDD
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xDE
  (DaliCommands_CommandPtr_t) DaliCommands_reservedFunction,                                //Command 0xDF

  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xE0
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xE1
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xE2
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xE3
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xE4
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xE5
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xE6
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xE7
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xE8
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xE9
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xEA
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xEB
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xEC
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xED
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xEE
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xEF

  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xF0
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xF1
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xF2
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xF3
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xF4
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xF5
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xF6
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xF7
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xF8
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xF9
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xFA
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xFB
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xFC
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xFD
  (DaliCommands_CommandPtr_t) DaliCommands_applicationExtendedCommands,                     //Command 0xFE
  (DaliCommands_CommandPtr_t) DaliCommands_queryExtendedVersionNumber,                      //Command 0xFF
};


//DaliCommands_SpecialCommands as Array of Function Pointers
//TODO Dali


//Interface Functions
CommLib_errorCode_e DaliCommands_onCommandReceived(uint8_t address, uint8_t data) {

    if (false == DaliCommands_isTalkingToMe(address)) {
        return CommLib_errorCode_wrongDaliFrameLength;
    }
    if (0 == (address & DALI_ADDRESS_SIGNIFICANT_BIT_BITMASK)) { //TODO Dali use some macro for this
        DaliCommands_onDirectCommand(data);
    }
    else {
        DaliCommands_onCommand(data);
    }
    return CommLib_errorCode_success;
}

//Commands

void DaliCommands_onCommand(uint8_t command) {
    DaliCommands_commandWasRepeated = (command == DaliCommands_lastCommand);
    DaliCommands_commandsArr[command](command);
    DaliCommands_lastCommand = command;
}
void DaliCommands_onDirectCommand(uint8_t command) {
    if (DALI_MASK == command) {
        return;
    }
    DaliCommands_dapc(command);
}
bool DaliCommands_isTalkingToMe(uint8_t address)  {
    //TODO Dali
    return false;
}

//Dali Commands

    /* 11.3 Level instructions */

void DaliCommands_dapc(uint8_t level) {
    /* 11.3.1 DAPC (level)
     * Upon execution of “DAPC (level)” (direct arc power control), “targetLevel” shall be calculated
     * on the basis of “level”.
     * The transition from “actualLevel” to “targetLevel” shall start using the applicable fade time.
     * Refer to subclauses 9.4, 9.7.3 and 9.13 for further information.
     */
    DaliCommands_controlLight(level, true);
}

void DaliCommands_off(void) {
    /*
     * 11.3.2 OFF
     * “targetLevel” shall be set to 0x00 and the lamp(s) shall switch off.
     * The transition from “actualLevel” to “targetLevel” shall be immediate and the light output shall
     * be adjusted as quickly as possible.
     * Refer to subclause 9.7.2 for further information.
     */
    DaliCommands_controlLight(0, false);
}
void DaliCommands_up(void) {
    /*
     * 11.3.3 UP
     * Dim up using a 200 ms fade with the set fade rate. “targetLevel” shall be calculated on the
     * basis of “actualLevel” and the set fade rate.
     * To ensure that there is a reaction to the command, at least one step (final “targetLevel” =
     * calculated “targetLevel”+1) shall be made upon execution of the first command of an iteration.
     * After that first step, the next steps shall be executed using the specified fade rate while the
     * fading is running. Every “UP” instruction executed as a part of an iteration shall cause the
     * 200 ms fade to be restarted and “targetLevel” to be recalculated on the basis of “actualLevel”
     * and the set fade rate.
     * There shall be no change to “actualLevel” if “actualLevel” is at “maxLevel” or 0x00.
     * Refer to subclauses 9.5.1, 9.7.3 and 9.8.2 for further information.
     */
    uint8_t targetLevel = DaliInternalStorage_variables.actualLevel + DaliInternalStorage_variables.fadeRate;
    DaliCommands_controlLight(targetLevel, true);
}
void DaliCommands_down(void){
    /*
     * 11.3.4 DOWN
     * Dim down using a 200 ms fade with the set fade rate. “targetLevel” shall be calculated on the
     * basis of “actualLevel” and the set fade rate.
     * To ensure that there is a reaction to the command, at least one step (final “targetLevel” =
     * calculated “targetLevel”-1) shall be made upon execution of the first command of an iteration.
     * After that first step, the next steps shall be executed using the specified fade rate while the
     * fading is running. Every “DOWN” instruction executed as a part of an iteration shall cause the
     * 200 ms fade to be restarted and “targetLevel” to be recalculated on the basis of “actualLevel”
     * and the set fade rate.
     * There shall be no change to “actualLevel” if “actualLevel” is at “minLevel” or 0x00.
     * Refer to subclauses 9.5.1, 9.7.3 and 9.8.2 for further information.
     */
    uint8_t targetLevel = DaliInternalStorage_variables.actualLevel - DaliInternalStorage_variables.fadeRate;
    DaliCommands_controlLight(targetLevel, true);
}
void DaliCommands_stepUp(void) {
    /*
     * 11.3.5 STEP UP
     * “targetLevel” shall be set to:
     * - if “targetLevel” = 0: 0x00
     * - if “minLevel” ≤ “targetLevel” < “maxLevel”: “targetLevel”+1
     * - if “targetLevel” = “maxLevel”: “maxLevel”
     * The transition from “actualLevel” to “targetLevel” shall be immediately and the light output
     * shall be adjusted as quickly as possible.
     * Refer to subclauses 9.4 and 9.5.9 for further information.
     */
    uint8_t targetLevel = DaliInternalStorage_variables.actualLevel + DaliInternalStorage_variables.fadeRate;
    DaliCommands_controlLight(targetLevel, false);

}
void DaliCommands_stepDown(void) {
    /*
     * 11.3.6 STEP DOWN
     * “targetLevel” shall be set to:
     * - if “targetLevel” = 0: 0x00
     * - if “minLevel” < “targetLevel” ≤ “maxLevel”: “targetLevel”-1
     * - if “targetLevel” = “minLevel”: “minLevel”
     * The transition from “actualLevel” to “targetLevel” shall be immediately and the light output
     * shall be adjusted as quickly as possible.
     * Refer to subclauses 9.4 and 9.5.9 for further information.
     */
    uint8_t targetLevel = DaliInternalStorage_variables.actualLevel + DaliInternalStorage_variables.fadeRate;
    DaliCommands_controlLight(targetLevel, false);
}
void DaliCommands_recallMaxLevel(void) {
    /*
     * 11.3.7 RECALL MAX LEVEL
     * When the “initialisationState” is DISABLED, “targetLevel” and “actualLevel” shall be set to
     * “maxLevel” immediately and the light output shall be adjusted as quickly as possible.
     * Refer to 9.7.2 for further information.
     * When the “initialisationState” is not DISABLED, the control gear shall set “actualLevel” and
     * “targetLevel” to “maxLevel”, and then adjust the light output as quickly as possible to 100 %
     * temporarily ignoring “maxLevel” and “actualLevel”.
     * If the device is unable to visually identify itself in this way, the control gear shall execute
     * “IDENTIFY DEVICE”, starting or re-triggering the identification procedure.
     * NOTE It is acceptable for the process of identifying individual control gear to depend upon RECALL MAX LEVEL
     * and RECALL MIN LEVEL commands being executed in an alternating sequence.
     * During identification no variables shall be affected except when explicitly stated otherwise.
     * Where appropriate, variables can be temporarily ignored, so that after the identification has
     * ended, there are no side effects.
     * Identification shall be stopped immediately when the “initialisationState” changes to
     * DISABLED and upon execution of any instruction other than INITIALISE (device),
     * RECALL MIN LEVEL, RECALL MAX LEVEL or IDENTIFY DEVICE.
     * When the “initialisationState” changes to DISABLED, the identification shall stop immediately.
     * Refer to 9.14.3 for further information.
     */
    uint8_t targetLevel = DaliInternalStorage_variables.maxLevel;
    DaliCommands_controlLight(targetLevel, false);

}
void DaliCommands_recallMinLevel(void) {
    /*
     * 11.3.8 RECALL MIN LEVEL
     * When the “initialisationState” is DISABLED, “targetLevel” and “actualLevel” shall be set to
     * “minLevel” immediately and the light output shall be adjusted as quickly as possible.
     * Refer to 9.7.2 for further information.
     * When “initialisationState” is not DISABLED, the control gear shall set “actualLevel” and
     * “targetLevel” to “minLevel” and then adjust the light output as quickly as possible to its PHM
     * level temporarily ignoring “minLevel” and “actualLevel”. If, however, PHM is not visibly
     * significantly different from 100 %, then the lamp shall be temporarily switched off instead.
     * If the device is unable to visually identify itself in this way, the control gear shall execute
     * “IDENTIFY DEVICE”, starting or re-triggering the identification procedure.
     * NOTE It is acceptable for the process of identifying individual control gear to depend upon RECALL MAX LEVEL
     * and RECALL MIN LEVEL commands being executed in an alternating sequence.
     * During identification no variables shall be affected except when explicitly stated otherwise.
     * Where appropriate, variables can be temporarily ignored, so that after the identification has
     * ended, there are no side effects.
     * Identification shall be stopped immediately when the “initialisationState” changes to
     * DISABLED and upon execution of any instruction other than INITIALISE (device),
     * RECALL MIN LEVEL, RECALL MAX LEVEL or IDENTIFY DEVICE.
     * When the “initialisationState” changes to DISABLED, identification shall stop immediately.
     * Refer to 9.14.3 for further information.
     */
    uint8_t targetLevel = DaliInternalStorage_variables.minLevel;
    DaliCommands_controlLight(targetLevel, false);
}
void DaliCommands_stepDownAndOff(void) {
    /*
     * 11.3.9 STEP DOWN AND OFF
     * “targetLevel” shall be set to:
     * - if “targetLevel” = 0: 0x00
     * - if “minLevel” < “targetLevel” ≤ “maxLevel”: “targetLevel”-1
     * - if “targetLevel” = “minLevel”: 0x00
     * The transition from “actualLevel” to “targetLevel” shall be immediately and the light output
     * shall be adjusted as quickly as possible.
     * Refer to subclauses 9.4 and 9.5.9 for further information.
     */
    uint8_t targetLevel = DaliInternalStorage_variables.actualLevel + DaliInternalStorage_variables.fadeRate;
    if (targetLevel > DaliInternalStorage_variables.minLevel) {
        targetLevel = 0;
    }
    DaliCommands_controlLight(targetLevel, false);
}
void DaliCommands_onAndStepUp(void) {
    /*
     * 11.3.10 ON AND STEP UP
     * “targetLevel” shall be set to:
     * - if “targetLevel” = 0: “minLevel”
     * - if “minLevel” ≤ “targetLevel” < “maxLevel”: “targetLevel”+1
     * - if “targetLevel” ≥ “maxLevel”: “maxLevel”
     * The transition from “actualLevel” to “targetLevel” shall be immediately and the light output
     * shall be adjusted as quickly as possible.
     * Refer to subclauses 9.4 and 9.5.9 for further information.
     */
    uint8_t targetLevel = DaliInternalStorage_variables.actualLevel + DaliInternalStorage_variables.fadeRate;
    if (DaliInternalStorage_variables.actualLevel == 0) {
        targetLevel = DaliInternalStorage_variables.minLevel;
    }
    DaliCommands_controlLight(targetLevel, false);
}
void DaliCommands_enableDAPCSequence(void) {
    /*
     * 11.3.11 ENABLE DAPC SEQUENCE
     * Indicates the start of a command iteration of “DAPC (level)” commands.
     * Refer to subclause 9.8.3 for further information.
     */
    //TODO Dali
}
void DaliCommands_goToLastActiveLevel(void) {
    /*
     * 11.3.12 GO TO LAST ACTIVE LEVEL
     * Upon execution of this command “targetLevel” shall be calculated based on “lastActiveLevel”.
     * The transition from “actualLevel” to “targetLevel” shall start using the set fade time.
     * Refer to subclauses 9.7.3 and 9.4 for further information.
     */
    uint8_t targetLevel = DaliInternalStorage_variables.lastActiveLevel;
    DaliCommands_controlLight(targetLevel, true);
}
void DaliCommands_continuousUp(void) {
    /*
     * 11.3.14 CONTINUOUS UP
     * Dim up using the set fade rate. “targetLevel” shall be set to “maxLevel” and a fade shall be
     * started using the set fade rate. The fade shall stop when “maxLevel” is reached.
     * There shall be no change to “actualLevel” if “actualLevel” is at “maxLevel” or 0x00.
     * Refer to 9.7.3 and 9.5.6.2 for further information.
     */
    if (0 == DaliInternalStorage_variables.actualLevel) {
        return;
    }
    uint8_t targetLevel = DaliInternalStorage_variables.maxLevel;
    DaliCommands_controlLight(targetLevel, true);
}
void DaliCommands_continuousDown(void) {
    /*
     * 11.3.15 CONTINUOUS DOWN
     * Dim down using the set fade rate. “targetLevel” shall be set to “minLevel” and a fade shall be
     * started using the set fade rate. The fade shall stop when “minLevel” is reached.
     * There shall be no change to “actualLevel” if “actualLevel” is at “minLevel” or 0x00.
     * Refer to 9.7.3 and 9.5.6.2 for further information.
     */
    if (0 == DaliInternalStorage_variables.actualLevel) {
        return;
    }
    uint8_t targetLevel = DaliInternalStorage_variables.minLevel;
    DaliCommands_controlLight(targetLevel, true);
}

void DaliCommands_goToScene(uint8_t sceneNumber) {
    /*
     * 11.3.13 GO TO SCENE (sceneNumber)
     * The control gear shall react depending on the actual value of “sceneX” where X is derived
     * from sceneNumber:
     * - if “sceneX” = MASK: the command shall not affect “targetLevel”;
     * - in all other cases: internally “DAPC (level)”, with level equal to “sceneX” shall be executed.
     * NOTE Using “DAPC (level)” implies the transition is made using the set fade time.
     * Refer to subclauses 9.19 and 11.3.1 for further information.
     */
    uint8_t sceneValue = DaliInternalStorage_variables.sceneX[DALI_HALFMASK & sceneNumber];
    if (DALI_MASK == sceneValue) {
        return;
    }
    DaliCommands_dapc(sceneValue);
}

    /* 11.4 Configuration instructions */
    /* 11.4.1 General
     * Device configuration instructions are used to change the configuration and/or the mode of
     * operation of the control gear. For this reason a device configuration instruction shall be
     * discarded, unless it is accepted twice according to the requirements as stated in IEC 62386-
     * 101:2014 and IEC62386-101:2014/AMD1:2018, 9.3.
     * Unless explicitly stated otherwise in the description of particular device configuration
     * instruction, the following holds:
     * - The instruction shall be ignored if so required by the provisions of subclause 9.7 of this
     * standard.
     * - The control gear shall not reply to the instruction.
     */

void DaliCommands_reset(void) {
    /*
     * 11.4.2 RESET
     * All variables shall be changed to their reset values. Control gear shall start to react properly
     * to commands no later than 300 ms after the execution of the instruction has been started.
     * If during a reset mains power fails, it is not guaranteed that “RESET” is completed.
     * Refer to subclause 9.11.1 and Table 14 for further information.
     */
    if (false == DaliCommands_commandWasRepeated) {
        return;
    }
    DaliInternalStorage_reset();
    DaliCommands_commandWasRepeated = false;
}
void DaliCommands_storeActualLevelInDTR0(void) {
    /*
     * 11.4.3 STORE ACTUAL LEVEL IN DTR0
     * The “actualLevel” shall be stored in “DTR0”.
     */
    if (false == DaliCommands_commandWasRepeated) {
        return;
    }
    DaliInternalStorage_variables.dtr0 = DaliInternalStorage_variables.actualLevel;
    DaliCommands_commandWasRepeated = false;
}
void DaliCommands_savePersistentVariables(void) {
    /*
     * 11.4.4 SAVE PERSISTENT VARIABLES
     * The control gear shall physically store all variables identified in Table 14 as non-volatile
     * memory (NVM). This shall include all application extended NVM variables defined in the
     * applicable parts 2xx.
     * The control gear might not react to commands after execution of this command. Control gear
     * shall start to react properly to commands no later than 300 ms after the execution of the
     * instruction has been started.
     * During processing of this command, the light output may fluctuate. After processing is
     * completed, the light output shall be at the level as expected before the execution of this
     * command, based on “targetLevel” and the transition that was active (if any).
     * This command is recommended to be used typically after commissioning. Due to the limited
     * number of write-cycles of persistent memory and due to the fact that there might be a visible
     * reaction, the control devices should limit the use of this command.
     * As there might be visual artefacts, it is recommended to use this command only during the off
     * state.
     * Refer to Table 14 and subclause 9.17 for further information.
     */
    if (false == DaliCommands_commandWasRepeated) {
        return;
    }
    //TODO Dali
    DaliCommands_commandWasRepeated = false;
}
void DaliCommands_setOperatingMode(void) {
    /*
     * 11.4.5 SET OPERATING MODE (DTR0)
     * “operatingMode” shall be set “DTR0”.
     * If “DTR0” does not correspond to an implemented operating mode, the command shall be
     * discarded.
     * Refer to subclause 9.9 for further information.
     */
    if (false == DaliCommands_commandWasRepeated) {
        return;
    }
    uint8_t operatingMode = DaliInternalStorage_variables.dtr0;
    if (operatingMode != 0 && operatingMode < 0x80) {
        //reserved operating mode, shall not be used
        return;
    }
    DaliInternalStorage_variables.operatingMode = operatingMode;
    DaliCommands_commandWasRepeated = false;
}
void DaliCommands_resetMemoryBank(void) {
    /*
     * 11.4.6 RESET MEMORY BANK (DTR0)
     * The command shall trigger the process to change the memory bank content to its reset values
     * as follows:
     * - if “DTR0” = 0: all implemented and unlocked memory banks except memory bank 0 shall
     * be reset
     * - in all other cases: the memory bank identified by “DTR0” shall be reset provided it is
     * implemented and unlocked
     * A memory bank needs to be unlocked to allow both lockable and non-lockable locations to be
     * reset.
     * Control gear shall start to react properly to commands no later than 10 s after the execution of
     * the instruction has been started.
     * Refer to subclause 9.11.2 for further information.
     */
    if (false == DaliCommands_commandWasRepeated) {
        return;
    }
    //TODO Dali
    DaliCommands_commandWasRepeated = false;
}
void DaliCommands_identifyDevice(void) {
    /*
     * 11.4.7 IDENTIFY DEVICE
     * The control gear shall start or restart a 10 s ±1 s timer. While the timer is running, a
     * procedure shall run which enables an observer to distinguish any control gear running this
     * process from any devices (of the same type) which are not running it. If the timer expires,
     * identification shall stop.
     * During identification no variables shall be affected except when explicitly stated otherwise.
     * Where appropriate, variables can be temporarily ignored, so that after the identification has
     * ended, there are no side effects.
     * When identification is active, the light output may be at any level between off and 100 %, MIN,
     * MAX and “actualLevel” being in effect temporarily ignored.
     * Identification shall be stopped immediately upon execution of any instruction other than
     * INITIALISE (device), RECALL MIN LEVEL, RECALL MAX LEVEL or IDENTIFY DEVICE.
     * While identification is active, the control gear shall, without interrupting the identification
     * procedure:
     * - on RECALL MIN LEVEL: set “actualLevel” and “targetLevel” to “minLevel”;
     * - on RECALL MAX LEVEL: set “actualLevel” and “targetLevel” to “maxLevel”.
     * When identification is stopped by an application controller, the corresponding timer shall be
     * cancelled immediately.
     * After identification has stopped, the light output shall be adjusted as quickly as possible to
     * reflect “actualLevel” and the command shall be executed (if applicable).
     * Identification can be used during commissioning in that it allows the installer to e.g. allocate
     * the particular identified device to a particular device group.
     * The indication can be done e.g. by flashing a LED, by producing a sound or other visual or
     * audible means. The exact process used to identify is manufacturer specific and should be
     * described in the manual.
     * NOTE The application controller can also stop the identification process using a “RESET” command.
     * Refer to subclause 9.14.3 for further information.
     */
    if (false == DaliCommands_commandWasRepeated) {
        return;
    }
    //TODO Dali
    DaliCommands_commandWasRepeated = false;
}
void DaliCommands_setMaxLevel(void) {
    /*
     * 11.4.8 SET MAX LEVEL (DTR0)
     * “maxLevel” shall be set to:
     * - if “minLevel” ≥ “DTR0”: “minLevel”
     * - if “DTR0” = MASK: 0xFE
     * - in all other cases: “DTR0”
     * If as a result of setting a new max level “actualLevel” > “maxLevel”, “targetLevel” shall be
     * calculated on the basis of “maxLevel”. The transition from “actualLevel” to “targetLevel” shall
     * start immediately and the light output shall be adjusted as quickly as possible.
     * Refer to subclause 9.7.2 for further information.
     */
    if (false == DaliCommands_commandWasRepeated) {
        return;
    }
    uint8_t maxLevel = DaliInternalStorage_variables.dtr0;
    if (DaliInternalStorage_variables.minLevel >= maxLevel) {
        maxLevel = DaliInternalStorage_variables.minLevel;
    }
    if (DALI_MASK == maxLevel) {
        maxLevel = 0xFE;
    }
    DaliInternalStorage_variables.maxLevel = maxLevel;
    DaliCommands_commandWasRepeated = false;
}
void DaliCommands_setMinLevel(void) {
    /*
     * 11.4.9 SET MIN LEVEL (DTR0)
     * “minLevel” shall be set to:
     * - if 0 ≤ “DTR0” ≤ PHM: PHM
     * - if “DTR0” ≥ “maxLevel” or MASK: “maxLevel”
     * - in all other cases: “DTR0”
     * If “actualLevel” > 0 and as a result of setting a new min level “actualLevel” < “minLevel”,
     * “targetLevel” shall be calculated on the basis of “minLevel”. The transition from “actualLevel”
     * to “targetLevel” shall be immediately and the light output shall be adjusted as quickly as
     * possible. Refer to subclause 9.7.2 for further information.
     */
    if (false == DaliCommands_commandWasRepeated) {
        return;
    }
    uint8_t minLevel = DaliInternalStorage_variables.dtr0;
    if (DaliInternalStorage_variables.phm >= minLevel) {
        minLevel = DaliInternalStorage_variables.phm;
    }
    if ((DALI_MASK == minLevel) || (minLevel > DaliInternalStorage_variables.maxLevel)) {
        minLevel = DaliInternalStorage_variables.maxLevel;
    }
    DaliInternalStorage_variables.minLevel = minLevel;
    DaliCommands_commandWasRepeated = false;
}
void DaliCommands_setSystemFailureLevel(void) {
    /*
     * 11.4.10 SET SYSTEM FAILURE LEVEL (DTR0)
     * “systemFailureLevel” shall be set to “DTR0”.
     * Refer to subclause 9.12 for further information.
     */
    if (false == DaliCommands_commandWasRepeated) {
        return;
    }
    DaliInternalStorage_variables.systemFailureLevel = DaliInternalStorage_variables.dtr0;
    DaliCommands_commandWasRepeated = false;
}
void DaliCommands_setPowerOnLevel(void) {
    /*
     * 11.4.11 SET POWER ON LEVEL (DTR0)
     * “powerOnLevel” shall be set to “DTR0”.
     * Refer to subclause 9.13 for further information.
     */
    if (false == DaliCommands_commandWasRepeated) {
        return;
    }
    DaliInternalStorage_variables.powerOnLevel = DaliInternalStorage_variables.dtr0;
    DaliCommands_commandWasRepeated = false;
}
void DaliCommands_setFadeTime(void) {
    /*
     * 11.4.12 SET FADE TIME (DTR0)
     * The “fadeTime” shall be set to a value according to the following steps:
     * - if “DTR0” > 15: 15
     * - in all other cases: “DTR0”
     * If “fadeTime” is not equal to 0, the fade time shall be calculated on the basis of “fadeTime”. If
     * “fadeTime” is equal to 0, the extended fade time shall be used.
     * If a new fade time is stored during a running fade process, this process shall be finished first
     * before the new value is used in the following fade.
     * Refer to subclauses 9.5, 9.7.3, 11.4.14 and 11.7.19 for further information.
     */
    if (false == DaliCommands_commandWasRepeated) {
        return;
    }
    uint8_t fadeTime = DaliInternalStorage_variables.dtr0;
    if (fadeTime > 15) {
        fadeTime = 15;
    }
    DaliInternalStorage_variables.fadeTime = fadeTime;
    DaliCommands_commandWasRepeated = false;
}
void DaliCommands_setFadeRate(void) {
    /*
     * 11.4.13 SET FADE RATE (DTR0)
     * The “fadeRate” shall be set to a value according to the following steps:
     * - if “DTR0” > 15: 15
     * - if “DTR0” = 0: 1
     * - in all other cases: “DTR0”
     * The fade rate shall be calculated on the basis of “fadeRate”. If a new fade rate is stored
     * during a running fade process, this process shall be finished first before the new value is used
     * in the following fade.
     * Refer to subclause 9.5 and 9.7.3 for further information.
     */
    if (false == DaliCommands_commandWasRepeated) {
        return;
    }
    uint8_t fadeRate = DaliInternalStorage_variables.dtr0;
    if (fadeRate > 15) {
        fadeRate = 15;
    }
    if (fadeRate == 0) {
        fadeRate = 1;
    }
    DaliInternalStorage_variables.fadeRate = fadeRate;
    DaliCommands_commandWasRepeated = false;
}
void DaliCommands_setExtendedFadeTime(void) {
    /*
     * 11.4.14 SET EXTENDED FADE TIME (DTR0)
     * The “extendedFadeTimeBase” and “extendedFadeTimeMultiplier” shall be set to a value
     * according to the following steps:
     * - If “DTR0” > 0x4F (0100 1111b):
     * – “extendedFadeTimeBase” shall be set to 0;
     * – “extendedFadeTimeMultiplier” shall be set to 0.
     * Effectively selecting a fade as quickly as possible.
     * - For all other cases:
     * – “extendedFadeTimeBase” shall be set to AAAAb where “DTR0” = xxxxAAAAb;
     * – “extendedFadeTimeMultiplier” shall be set to YYYb where “DTR0” = xYYYxxxxb.
     * - The fade time shall be calculated by multiplying the base value and the multiplier.
     * If a new fade time is stored during a running fade process, this process shall be finished first
     * before the new value is used in the following fade.
     * Refer to subclause 0 for further information.
     */
    if (false == DaliCommands_commandWasRepeated) {
        return;
    }
    uint8_t extendedFadeRateBase = 0;
    uint8_t extendedFadeRateMultiplier = 0;
    if (DaliInternalStorage_variables.dtr0 <= 0x4F) {
        extendedFadeRateBase = (DaliInternalStorage_variables.dtr0 & 0b00001111);
        extendedFadeRateMultiplier = (DaliInternalStorage_variables.dtr0 & 0b01110000);
        //TODO  Dali #defines
    }
    DaliInternalStorage_variables.extendedFadeTimeBase = extendedFadeRateBase;
    DaliInternalStorage_variables.extendedFadeTimeMultiplier = extendedFadeRateMultiplier;
    DaliCommands_commandWasRepeated = false;
}

void DaliCommands_setScene(uint8_t sceneX) {
    /*
     * 11.4.15 SET SCENE (DTR0, sceneX)
     * This command actually comprises 16 commands, one for each scene. This is accomplished
     * by selecting a block of 16 consecutive opcodes.
     * Upon execution of “SET SCENE (DTR0, sceneX)”, the scene number shall be derived from the
     * opcode:sceneNumber = opcode – 0x40. This identifies the “sceneX” to be used.
     * “sceneX” shall be set to “DTR0”.
     * Refer to subclause 9.19 for further information
     */
    if (false == DaliCommands_commandWasRepeated) {
        return;
    }
    uint8_t sceneNumber = (sceneX & DALI_HALFMASK);
    DaliInternalStorage_variables.sceneX[sceneNumber] = DaliInternalStorage_variables.dtr0;
    DaliCommands_commandWasRepeated = false;
}

void DaliCommands_removeFromScene(uint8_t sceneX) {
    /*
     * 11.4.16 REMOVE FROM SCENE (sceneX)
     * This command actually comprises 16 commands, one for each scene. This is accomplished
     * by selecting a block of 16 consecutive opcodes.
     * Upon execution of “REMOVE FROM SCENE (sceneX), the scene number shall be derived from
     * the opcode: sceneNumber = opcode – 0x50. This identifies the “sceneX” to be used.
     * “sceneX” shall be set to MASK. This effectively removes the control gear as member from the
     * scene.
     * Refer to subclause 9.19 for further information.
     */
    if (false == DaliCommands_commandWasRepeated) {
        return;
    }
    uint8_t sceneNumber = (sceneX & DALI_HALFMASK);
    DaliInternalStorage_variables.sceneX[sceneNumber] = DALI_MASK;
    DaliCommands_commandWasRepeated = false;
}

void DaliCommands_addToGroup(uint8_t group) {
    /*
     * 11.4.17 ADD TO GROUP (group)
     * This command actually comprises 16 commands, one for each group. This is accomplished by
     * selecting a block of 16 consecutive opcodes.
     * Upon execution of “ADD TO GROUP (group)”, group shall be derived from the opcode:
     * group = opcode – 0x60. This identifies the group to be used.
     * bit[group] of “gearGroups” shall be set to TRUE. This implies that the control gear is a
     * member of this group.
     */
    if (false == DaliCommands_commandWasRepeated) {
        return;
    }
    uint8_t groupNumber = (group & DALI_HALFMASK);
    DaliInternalStorage_variables.gearGroups |= (1U << groupNumber);
    DaliCommands_commandWasRepeated = false;
}

void DaliCommands_removeFromGroup(uint8_t group) {
    /*
     * 11.4.18 REMOVE FROM GROUP (group)
     * This command actually comprises 16 commands, one for each group. This is accomplished by
     * selecting a block of 16 consecutive opcodes.
     * Upon execution of “REMOVE FROM GROUP (group)”, group shall be derived from the opcode:
     * group = opcode – 0x70. This identifies the group to be used.
     * bit[group] of “gearGroups” shall be set to FALSE. This implies that the control gear is not a
     * member of this group.
     */
    if (false == DaliCommands_commandWasRepeated) {
        return;
    }
    uint8_t groupNumber = (group & DALI_HALFMASK);
    DaliInternalStorage_variables.gearGroups &= ~(1U << groupNumber);
    DaliCommands_commandWasRepeated = false;
}

void DaliCommands_setShortAddress(void) {
    /*
     * 11.4.19 SET SHORT ADDRESS (DTR0)
     * “shortAddress” shall be set to:
     * - if “DTR0” = MASK: MASK (effectively deleting the short address);
     * - if “DTR0” = 1xxxxxxxb or xxxxxxx0b: no change;
     * - in all other cases (0AAAAAA1b): 00AAAAAAb.
     */
    if (false == DaliCommands_commandWasRepeated) {
        return;
    }
    if (DALI_MASK == DaliInternalStorage_variables.dtr0) {
        DaliInternalStorage_variables.shortAddress = DALI_MASK;
    }
    //TODO Dali verify this logic
    if ((0 != (DaliInternalStorage_variables.dtr0 & 0b10000000)) || (0 == (DaliInternalStorage_variables.dtr0 & 0b00000001))) {
        uint8_t shortAddress = (DaliInternalStorage_variables.dtr0 > 1) & 0b00111111;
        DaliInternalStorage_variables.shortAddress = shortAddress;
    }
    DaliCommands_commandWasRepeated = false;
}
void DaliCommands_enableWriteMemory(void) {
    /*
     * 11.4.20 ENABLE WRITE MEMORY
     * “writeEnableState” shall be set to ENABLED.
     * NOTE There is no command to explicitly disable memory write access, since any command that is not directly
     * involved with writing into memory banks will automatically set “writeEnableState” to DISABLED.
     * Refer to subclause 9.10.5 for further information.
     */
    if (false == DaliCommands_commandWasRepeated) {
        return;
    }
    //TODO Dali
    DaliCommands_commandWasRepeated = false;
}

    /* 11.5 Queries */
    /* 11.5.1 General
     * Queries are used to retrieve property values from a control gear. The addressed control gear
     * returns the queried property value in a backward frame.
     * Unless explicitly stated otherwise in the description of a particular query, the following holds:
     * - The query shall be ignored if so required by the provisions of subclause 9.7.
     * When applicable, the query shall be discarded if any of the parameter values (in “DTR0”,
     * “DTR1” and “DTR2”) are outside the range of validity of the addressed device variables, as
     * given in Table 14.
     */

void DaliCommands_queryStatus(void) {
    /*
     * 11.5.2 QUERY STATUS
     * The answer shall be the status, which is formed by a combination of control gear properties.
     * Refer to subclause 9.16 for further information.
     */
    //TODO Dali  use #defines
    uint8_t backwardFrame = 0x00;
    if (DALI_TRUE == DaliInternalStorage_variables.controlGearFailure) {
        backwardFrame &= (1U << 0);
    }
    if (DALI_TRUE == DaliInternalStorage_variables.lampFailure) {
        backwardFrame &= (1U << 1);
    }
    if (DALI_TRUE == DaliInternalStorage_variables.lampOn) {
        backwardFrame &= (1U << 2);
    }
    if (DALI_TRUE == DaliInternalStorage_variables.limitError) {
        backwardFrame &= (1U << 3);
    }
    if (DALI_TRUE == DaliInternalStorage_variables.fadeRunning) {
        backwardFrame &= (1U << 4);
    }
    if (DALI_TRUE == DaliInternalStorage_variables.resetState) {
        backwardFrame &= (1U << 5);
    }
    if (DALI_MASK == DaliInternalStorage_variables.shortAddress) {
        backwardFrame &= (1U << 6);
    }
    if (DALI_TRUE == DaliInternalStorage_variables.powerCycleSeen) {
        backwardFrame &= (1U << 7);
    }
    CommLib_executeDaliWriteCallback(backwardFrame);
}
void DaliCommands_queryControlGearPresent(void) {
    /*
     * 11.5.3 QUERY CONTROL GEAR PRESENT
     * The answer shall be YES.
     */
    uint8_t backwardFrame = 0x01;
    CommLib_executeDaliWriteCallback(backwardFrame);
}

void DaliCommands_queryLampFailure(void) {
    /*
     * 11.5.5 QUERY LAMP FAILURE
     * The answer shall be YES if “lampFailure” is TRUE and NO otherwise.
     */
    uint8_t backwardFrame = DaliInternalStorage_variables.lampFailure;
    CommLib_executeDaliWriteCallback(backwardFrame);
}
void DaliCommands_queryLampPowerOn(void) {
    /*
     * 11.5.6 QUERY LAMP POWER ON
     * The answer shall be YES if “lampOn” is TRUE and NO otherwise.
     */
    uint8_t backwardFrame = DaliInternalStorage_variables.lampOn;
    CommLib_executeDaliWriteCallback(backwardFrame);
}
void DaliCommands_queryLimitError(void) {
    /*
     * 11.5.7 QUERY LIMIT ERROR
     * The answer shall be YES if “limitError” is TRUE and NO otherwise.
     */
    uint8_t backwardFrame = DaliInternalStorage_variables.limitError;
    CommLib_executeDaliWriteCallback(backwardFrame);
}
void DaliCommands_queryResetState(void) {
    /*
     * 11.5.8 QUERY RESET STATE
     * The answer shall be YES if “resetState” is TRUE and NO otherwise.
     */
    uint8_t backwardFrame = DaliInternalStorage_variables.resetState;
    CommLib_executeDaliWriteCallback(backwardFrame);

}
void DaliCommands_queryMissingShortAddress(void) {
    /*
     * 11.5.9 QUERY MISSING SHORT ADDRESS
     * The answer shall be YES if “shortAddress” is equal to MASK and NO otherwise.
     * NOTE Since the control gear answers only if no short address is stored, the use of the command is useful only in
     * broadcast mode or if group addressing is used.
     */
    uint8_t backwardFrame = (DALI_MASK == DaliInternalStorage_variables.shortAddress) ? (DALI_TRUE) : (DALI_FALSE);
    CommLib_executeDaliWriteCallback(backwardFrame);
}
void DaliCommands_queryVersionNumber(void) {
    /*
     * 11.5.10 QUERY VERSION NUMBER
     * The answer shall be the content of memory bank 0 location 0x16.
     * Refer to Clause 4 and Table 9 for further information.
     */
    //TODO Dali
}
void DaliCommands_queryContentDTR0(void) {
    /*
     * 11.5.11 QUERY CONTENT DTR0
     * The answer shall be “DTR0”.
     */
    uint8_t backwardFrame = DaliInternalStorage_variables.dtr0;
    CommLib_executeDaliWriteCallback(backwardFrame);
}
void DaliCommands_queryDeviceType(void) {
    /*
     * 11.5.12 QUERY DEVICE TYPE
     * The answer shall be:
     * - if no Part 2xx is implemented: 254;
     * - if one device type/feature is supported: the device type/feature number;
     * - if more than one device type/feature is supported: MASK.
     * The coding of the device types/features shall be as specified in the particular Parts 2xx of IEC
     * 62386.
     * Refer to subclauses 9.18 and 11.5.13 for further information.
     */
    //TODO Dali
}
void DaliCommands_queryPhysicalMinimum(void) {
    /*
     * 11.5.14 QUERY PHYSICAL MINIMUM
     * The answer shall be PHM.
     */
    uint8_t backwardFrame = DaliInternalStorage_variables.phm;
    CommLib_executeDaliWriteCallback(backwardFrame);
}
void DaliCommands_queryPowerFailure(void) {
    /*
     * 11.5.15 QUERY POWER FAILURE
     * The answer shall be YES if “powerCycleSeen” is TRUE and NO otherwise.
     */
    uint8_t backwardFrame = DaliInternalStorage_variables.powerCycleSeen;
    CommLib_executeDaliWriteCallback(backwardFrame);
}
void DaliCommands_queryContentDTR1(void) {
    /*
     * 11.5.16 QUERY CONTENT DTR1
     * The answer shall be “DTR1”.
     */
    uint8_t backwardFrame = DaliInternalStorage_variables.dtr1;
    CommLib_executeDaliWriteCallback(backwardFrame);
}
void DaliCommands_queryOperatingMode(void) {
    /*
     * 11.5.18 QUERY OPERATING MODE
     * The answer shall be “operatingMode”.
     * Refer to subclause 9.9 for further information.
     */
    uint8_t backwardFrame = DaliInternalStorage_variables.operatingMode;
    CommLib_executeDaliWriteCallback(backwardFrame);
}
void DaliCommands_queryLightSourceType(void) {
    /*
     * 11.5.19 QUERY LIGHT SOURCE TYPE
     * The answer shall be the number of the light source type given in Table 17.
     * Table 17 – Light source type encoding
     * +-------------------------------+----------------+------------------------------+
     * | Type of light source          | Encoding       |    Lamp failure detection    |
     * +-------------------------------+----------------+------------------------------+
     * |                               |                | Open circuit | Short circuit |
     * +===============================+================+==============+===============+
     * | Low pressure fluorescent      | 0              | YES          |               |
     * +-------------------------------+----------------+--------------+---------------+
     * | HID                           | 2              | YES          |               |
     * +-------------------------------+----------------+--------------+---------------+
     * | Low voltage halogen           | 3              | YES          |               |
     * +-------------------------------+----------------+--------------+---------------+
     * | Incandescent                  | 4              | YES          |               |
     * +-------------------------------+----------------+--------------+---------------+
     * | LED                           | 6              | YES [a]      | YES [a]       |
     * +-------------------------------+----------------+--------------+---------------+
     * | OLED                          | 7              | YES [a]      | YES [a]       |
     * +-------------------------------+----------------+--------------+---------------+
     * | Other than listed above       | 252            | YES          |               |
     * +-------------------------------+----------------+--------------+---------------+
     * | Unknown light source type [b] | 253            | YES [d]      | [d]           |
     * +-------------------------------+----------------+--------------+---------------+
     * | No light source [c]           | 254            | [d]          | [d]           |
     * +-------------------------------+----------------+--------------+---------------+
     * | Multiple light source types   | MASK           | YES [e]      | YES [e]       |
     * +-------------------------------+----------------+--------------+---------------+
     * | Reserved                      | 1, 5, (8, 251) |              |               |
     * +-------------------------------+----------------+--------------+---------------+
     * | [a]: Testing shall be done with light output of at least 5%                   |
     * | [b]: Typically used in the case of signal conversion, for example 1V to 10V   |
     * | [c]: Used in cases where no light source is connected, for example a relay    |
     * | [d]: See 9.16.3                                                               |
     * | [e]: Depending on the supported light source types                            |
     * +-------------------------------+----------------+--------------+---------------+
     * When MASK is answered the content of DTR0 shall contain a value representing the first light
     * source type, DTR1 shall represent the second light source type, and DTR2 shall represent the
     * third light source type.
     * When exactly two different light source types are available, DTR2 shall contain 254, indicating
     * "no light source".
     */
    //TODO Dali
}

void DaliCommands_queryContentDTR2(void) {
    /*
     * 11.5.17 QUERY CONTENT DTR2
     * The answer shall be “DTR2”.
     */
    uint8_t backwardFrame = DaliInternalStorage_variables.dtr2;
    CommLib_executeDaliWriteCallback(backwardFrame);
}
void DaliCommands_queryActualLevel(void) {
    /*
     * 11.5.20 QUERY ACTUAL LEVEL
     * The answer shall be:
     * - if “actualLevel” = 0x00: 0x00 (see also 9.13);
     * - In all other cases:
     * – during startup: MASK;
     * – no light output (e.g. due to total lamp failure, control gear failure) while light output is
     * expected: MASK;
     * – in all other cases: “actualLevel”.
     */
    uint8_t backwardFrame = DaliInternalStorage_variables.actualLevel;
    //TODO  Dali add other cases
    CommLib_executeDaliWriteCallback(backwardFrame);
}
void DaliCommands_queryMaxLevel(void) {
    /*
     * 11.5.21 QUERY MAX LEVEL
     * The answer shall be “maxLevel”.
     */
    uint8_t backwardFrame = DaliInternalStorage_variables.maxLevel;
    CommLib_executeDaliWriteCallback(backwardFrame);
}
void DaliCommands_queryMinLevel(void) {
    /*
     * 11.5.22 QUERY MIN LEVEL
     * The answer shall be “minLevel”.
     */
    uint8_t backwardFrame = DaliInternalStorage_variables.minLevel;
    CommLib_executeDaliWriteCallback(backwardFrame);
}
void DaliCommands_queryPowerOnLevel(void) {
    /*
     * 11.5.23 QUERY POWER ON LEVEL
     * The answer shall be “powerOnLevel”.
     * Refer to subclause 9.12 for further information.
     */
    uint8_t backwardFrame = DaliInternalStorage_variables.powerOnLevel;
    CommLib_executeDaliWriteCallback(backwardFrame);
}
void DaliCommands_querySystemFailureLevel(void) {
    /*
     * 11.5.24 QUERY SYSTEM FAILURE LEVEL
     * The answer shall be “systemFailureLevel”.
     * Refer to subclause 9.12 for further information.
     */
    uint8_t backwardFrame = DaliInternalStorage_variables.systemFailureLevel;
    CommLib_executeDaliWriteCallback(backwardFrame);
}
void DaliCommands_queryFadeTimeFadeRate(void) {
    /*
     * 11.5.25 QUERY FADE TIME/FADE RATE
     * The answer shall be XXXX YYYYb, where XXXXb equals “fadeTime” and YYYYb equals
     * “fadeRate”.
     */
    uint8_t backwardFrame = (0b11110000 & DaliInternalStorage_variables.fadeTime);
    backwardFrame |= (0b00001111 & DaliInternalStorage_variables.fadeRate);
    CommLib_executeDaliWriteCallback(backwardFrame);
}
void DaliCommands_queryManufacturerSpecificMode(void) {
    /*
     * 11.5.27 QUERY MANUFACTURER SPECIFIC MODE
     * The answer shall be YES when “operatingMode” is in the range [0x80,0xFF] and NO
     * otherwise.
     */
    uint8_t backwardFrame = DALI_FALSE;
    if (DaliInternalStorage_variables.operatingMode >= 0x80) {
        backwardFrame = DALI_TRUE;
    }
    CommLib_executeDaliWriteCallback(backwardFrame);
}
void DaliCommands_queryNextDeviceType(void) {
    /*
     * 11.5.13 QUERY NEXT DEVICE TYPE
     * The answer shall be:
     * - if directly preceded by “QUERY DEVICE TYPE”, and more than one device type/feature is
     * supported: the first and lowest device type/feature number;
     * - if directly preceded by “QUERY NEXT DEVICE TYPE”, and not all device types/features
     * have been reported: the next lowest device type/feature number;
     * - if directly preceded by “QUERY NEXT DEVICE TYPE”, and all device types/features have
     * been reported: 254;
     * - in all other cases: NO.
     * The sequence of commands shall only be accepted as long as they use the same address
     * byte. Multi-master transmitters shall send such sequence as a transaction. The coding of the
     * device types/features shall be as specified in the particular Parts 2xx of IEC 62386.
     * Refer to subclause 9.18 and 11.5.12 for further information.
     */
    //TODO Dali
}
void DaliCommands_queryExtendedFadeTime(void) {
    /*
     * 11.5.26 QUERY EXTENDED FADE TIME
     * The answer shall be 0 XXX YYYYb, where XXXb equals “extendedFadeTimeMultiplier” and
     * YYYYb equals “extendedFadeTimeBase”.
     */
    uint8_t backwardFrame = (0b01110000 & DaliInternalStorage_variables.extendedFadeTimeMultiplier);
    backwardFrame |= (0b00001111 & DaliInternalStorage_variables.extendedFadeTimeBase);
}
void DaliCommands_queryControlGearFailure(void) {
    /*
     * 11.5.4 QUERY CONTROL GEAR FAILURE
     * The answer shall be YES if “controlGearFailure” is TRUE and NO otherwise.
     */
    uint8_t backwardFrame = DaliInternalStorage_variables.controlGearFailure;
    CommLib_executeDaliWriteCallback(backwardFrame);
}

void DaliCommands_querySceneLevel(uint8_t scenIdx) {
    /*
     * 11.5.28 QUERY SCENE LEVEL (sceneX)
     * This command actually comprises 16 commands, one for each scene. This is accomplished
     * by selecting a block of 16 consecutive opcodes.
     * Upon execution of “QUERY SCENE LEVEL (sceneX)”, the scene number shall be derived from
     * the opcode: sceneNumber = opcode – 0xB0. This identifies the “sceneX” to be used.
     * The answer shall be “sceneX”.
     * Refer to subclause 9.19 for further information.
     */
    uint8_t sceneNumber = scenIdx & DALI_HALFMASK;
    uint8_t backwardFrame = DaliInternalStorage_variables.sceneX[sceneNumber];
    CommLib_executeDaliWriteCallback(backwardFrame);
}

void DaliCommands_queryGroups0To7(void) {
    /*
     * 11.5.29 QUERY GROUPS 0-7
     * The answer shall be “gearGroups[7:0]”.
     * The membership of groups 0-7 shall be represented as an 8-bit value, with one bit for each
     * group. “0” shall be interpreted as not a member, and “1” shall be interpreted as member of the
     * group. Bit[X] shall represent membership of group X, where X is in the range [0,7].
     */
    uint8_t backwardFrame = (uint8_t) (DaliInternalStorage_variables.gearGroups & 0xFF);
    CommLib_executeDaliWriteCallback(backwardFrame);
}
void DaliCommands_queryGroups8To15(void) {
    /*
     * 11.5.30 QUERY GROUPS 8-15
     * The answer shall be “gearGroups[15:8]”.
     * The membership of groups 8-15 shall be represented as an 8-bit value, with one bit for each
     * group. “0” shall be interpreted as not a member, and “1” shall be interpreted as member of the
     * group. Bit[X] shall represent membership of group X+8, where X is in the range [0,7].
     */
    uint8_t backwardFrame = (uint8_t) ((DaliInternalStorage_variables.gearGroups & 0xFF00) >> 8);
    CommLib_executeDaliWriteCallback(backwardFrame);

}
void DaliCommands_queryRandomAddressH(void) {
    /*
     * 11.5.31 QUERY RANDOM ADDRESS (H)
     * The answer shall be “randomAddress[23:16]”.
     */
    uint8_t backwardFrame = DaliInternalStorage_variables.randomAddressHighBit;
    CommLib_executeDaliWriteCallback(backwardFrame);
}
void DaliCommands_queryRandomAddressM(void) {
    /*
     * 11.5.32 QUERY RANDOM ADDRESS (M)
     * The answer shall be “randomAddress[15:8]”.
     */
    uint8_t backwardFrame = DaliInternalStorage_variables.randomAddressMidBit;
    CommLib_executeDaliWriteCallback(backwardFrame);
}
void DaliCommands_queryRandomAddressL(void) {
    /*
     * 11.5.33 QUERY RANDOM ADDRESS (L)
     * The answer shall be “randomAddress[7:0]”.
     */
    uint8_t backwardFrame = DaliInternalStorage_variables.randomAddressLowBit;
    CommLib_executeDaliWriteCallback(backwardFrame);
}
void DaliCommands_readMemoryLocation(void) {
    /*
     * 11.5.34 READ MEMORY LOCATION (DTR1, DTR0)
     * The query shall be discarded if the addressed memory bank is not implemented.
     * If executed, the answer shall be the content of the memory location identified by “DTR0”
     * within memory bank “DTR1”.
     * The control gear shall answer NO if the addressed memory location is not implemented.
     * NOTE 1 This allows holes in the memory bank implementation.
     * If the addressed location is below location 0xFF, the control gear shall increment “DTR0” by
     * one.
     * NOTE 2 This allows efficient multi-byte reading within a transaction.
     * Refer to subclause 9.10 for further information.
     */
    //TODO Dali
}
void DaliCommands_applicationExtendedCommands(void) {
    /*
     * 11.6.1 General
     * “ENABLE DEVICE TYPE (data)” shall be executed before an application extended command
     * to enable the correct device type/feature command set. For further requirements, see
     * command “ENABLE DEVICE TYPE (data)” and 11.7.14.
     * If “ENABLE DEVICE TYPE (data)” is discarded or not received directly before an application
     * extended command is accepted, the application extended command shall be discarded.
     * The definition of the extended commands is part of the application specific standards.
     * Refer to 9.18 for further information.
     */
    //TODO Dali
}
void DaliCommands_queryExtendedVersionNumber(void) {
    /*
     * 11.6.2 QUERY EXTENDED VERSION NUMBER
     * The answer shall be the version number of Part 2xx of this standard for the corresponding
     * device type/feature as an 8-bit number.
     * The answer shall be:
     * - if the enabled device type/feature is not implemented: NO;
     * - if the enabled device type/feature is supported: the version number belonging to the
     * device type/feature number.
     * Refer to subclause 9.18 for further information.
     */
    //TODO Dali
}

    /*
     * 11.7 Special commands
     * 11.7.1 General
     * All special mode commands shall be interpreted as instructions unless explicitly stated
     * otherwise.
     */

void DaliCommands_SpecialCommands_terminate(void) {
    /*
     * 11.7.2 TERMINATE
     * The following processes shall be terminated immediately upon execution of this instruction:
     * - Initialisation, “initialisationState” shall be set to DISABLED.
     * - Identification, whether started as part of initialisation (using RECALL MAX LEVEL,
     * RECALL MIN LEVEL) or as a standard operation (IDENTIFY DEVICE) shall be stopped.
     * The command could also terminate other processes as identified in the relevant 2xx parts.
     * Refer to subclause 9.14.2 for further information.
     */
    //TODO Dali
}
void DaliCommands_SpecialCommands_DTR0(void) {
    /*
     * 11.7.3 DTR0 (data)
     * “DTR0” shall be set to given data.
     * Refer to subclause 9.10 for further information.
     */
    //TODO Dali
}
void DaliCommands_SpecialCommands_initialise(void) {

}
void DaliCommands_SpecialCommands_randomise(void) {

}
void DaliCommands_SpecialCommands_compare(void) {

}
void DaliCommands_SpecialCommands_withdraw(void) {

}
void DaliCommands_SpecialCommands_ping(void) {

}
void DaliCommands_SpecialCommands_searchAddrH(void) {

}
void DaliCommands_SpecialCommands_searchAddrM(void) {

}
void DaliCommands_SpecialCommands_searchAddrL(void) {

}
void DaliCommands_SpecialCommands_programShortAddress(void) {

}
void DaliCommands_SpecialCommands_verifyShortAddress(void) {

}
void DaliCommands_SpecialCommands_enableDeviceType(void) {

}
void DaliCommands_SpecialCommands_DTR1(void) {

}
void DaliCommands_SpecialCommands_DTR2(void) {

}
void DaliCommands_SpecialCommands_writeMemoryLocation(void) {

}
void DaliCommands_SpecialCommands_writeMemoryLocationNoReply(void) {

}

void DaliCommands_reservedSpecialFunction(uint8_t cmd, uint8_t data_val) {

}

void DaliCommands_reservedFunction(void) {
    //Do nothing
    return;
}

void DaliCommands_controlLight(uint8_t targetLevel, bool fadeEnabled) {
    //TODO Dali
}

#endif //NETWORK_DALI_SUPPORT == ENABLED

