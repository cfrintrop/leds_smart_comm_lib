#ifndef SRC_COMMLIB_H_
#define SRC_COMMLIB_H_

#include "CommLib_Definitions.h"

CommLib_errorCode_e CommLib_setCallbacks(CommLib_callbacks_t *_pCallbacks);

void CommLib_setCurrentPWMValue(CommLib_pwmValue_t pwmValue);
CommLib_pwmValue_t CommLib_getCurrentPWMValue(void);

void CommLib_setLightStatus(bool lightIsOn);
bool CommLib_getLightStatus(void);

CommLib_errorCode_e CommLib_executePWMChangeColorCallback(CommLib_pwmValue_t pwmValue);
CommLib_errorCode_e CommLib_executePWMTurnOnCallback(void);
CommLib_errorCode_e CommLib_executePWMTurnOffCallback(void);

CommLib_errorCode_e CommLib_executeUpdateAlexaCallback(CommLib_AlexaLightValues_t lightValue);
CommLib_errorCode_e CommLib_executeUpdateHomekitCallback(CommLib_HomekitLightValues_t lightValue);

#if (NETWORK_DALI_SUPPORT == ENABLED)
CommLib_errorCode_e CommLib_executeDaliWriteCallback(uint8_t backwardFrame);
#endif //NETWORK_DALI_SUPPORT == ENABLED

#endif /* SRC_COMMLIB_H_ */
