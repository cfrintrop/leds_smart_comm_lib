#ifndef SRC_COLORCONVERSION_COLORCONVERSION_H_
#define SRC_COLORCONVERSION_COLORCONVERSION_H_

#include "CommLib_Definitions.h"
#include "ColorConversion_Definitions.h"

CommLib_pwmValue_t ColorConversion_AlexaToPWM(CommLib_AlexaLightValues_t input);
CommLib_AlexaLightValues_t ColorConversion_PWMToAlexa(CommLib_pwmValue_t input);

CommLib_pwmValue_t ColorConversion_HomekitToPWM(CommLib_HomekitLightValues_t input);
CommLib_HomekitLightValues_t ColorConversion_PWMToHomekit(CommLib_pwmValue_t input);

CommLib_HomekitLightValues_t ColorConversion_AlexaToHomekit(CommLib_AlexaLightValues_t input);
CommLib_AlexaLightValues_t ColorConversion_HomekitToAlexa(CommLib_HomekitLightValues_t input);

#if (COLOR_CONVERSION_DEBUG_PRINTS == ENABLED)
#include "stdio.h"
void ColorConversion_printAlexaLightValues(CommLib_AlexaLightValues_t alexaLightValues);
void ColorConversion_printHomekitLightValues(CommLib_HomekitLightValues_t homekitLightValues);
void ColorConversion_printPWMValues(CommLib_pwmValue_t pwmLightValues);
#endif //COLOR_CONVERSION_DEBUG_PRINTS == ENABLED

#endif /* SRC_COLORCONVERSION_COLORCONVERSION_H_ */

