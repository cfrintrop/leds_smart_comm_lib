################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/Dali/DaliCommands.c \
../src/Dali/DaliInternalStorage.c 

OBJS += \
./src/Dali/DaliCommands.o \
./src/Dali/DaliInternalStorage.o 

C_DEPS += \
./src/Dali/DaliCommands.d \
./src/Dali/DaliInternalStorage.d 


# Each subdirectory must supply rules for building sources it contributes
src/Dali/%.o: ../src/Dali/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cygwin C Compiler'
	gcc -DUNIT_TESTING -I"C:\Workspaces\CWorkspace\LEDsSMART_CommLib\extern" -I"C:\Workspaces\CWorkspace\LEDsSMART_CommLib\src" -I"C:\Workspaces\CWorkspace\LEDsSMART_CommLib\src\ColorConversion" -I"C:\Workspaces\CWorkspace\LEDsSMART_CommLib\src\Dali" -I"C:\Workspaces\CWorkspace\LEDsSMART_CommLib\tests" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


