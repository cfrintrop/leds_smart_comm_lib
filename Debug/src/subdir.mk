################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/CommLib.c \
../src/CommLib_Interface.c 

OBJS += \
./src/CommLib.o \
./src/CommLib_Interface.o 

C_DEPS += \
./src/CommLib.d \
./src/CommLib_Interface.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cygwin C Compiler'
	gcc -DUNIT_TESTING -I"C:\Workspaces\CWorkspace\LEDsSMART_CommLib\extern" -I"C:\Workspaces\CWorkspace\LEDsSMART_CommLib\src" -I"C:\Workspaces\CWorkspace\LEDsSMART_CommLib\src\ColorConversion" -I"C:\Workspaces\CWorkspace\LEDsSMART_CommLib\src\Dali" -I"C:\Workspaces\CWorkspace\LEDsSMART_CommLib\tests" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


