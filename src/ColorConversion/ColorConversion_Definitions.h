#ifndef SRC_COLORCONVERSION_COLORCONVERSION_DEFINITIONS_H_
#define SRC_COLORCONVERSION_COLORCONVERSION_DEFINITIONS_H_

#if (PWM_TYPE == DIMMABLE_RGB)
#define INITIAL_PWM_VALUE       \
{                               \
    .redValue = 0,              \
    .greenValue = 0,            \
    .blueValue = 0,             \
};

#define INITIAL_ALEXA_VALUE     \
{                               \
    .isOn = true,               \
    .hue = 0,                   \
    .saturation = 0,            \
    .value = 0,                 \
};

#define INITIAL_HOMEKIT_VALUE   \
{                               \
    .isOn = true,               \
    .hue = 0,                   \
    .saturation = 0,            \
    .brightness = 0,            \
};
#endif //PWM_TYPE == DIMMABLE_RGB

#if (PWM_TYPE == DIMMABLE_WHITE)
#define INITIAL_PWM_VALUE       \
{                               \
    .brightnessValue = 0,       \
};

#define INITIAL_ALEXA_VALUE     \
{                               \
    .isOn = true,               \
    .brightnessValue = 0,       \
};

#define INITIAL_HOMEKIT_VALUE   \
{                               \
    .isOn = true,               \
    .brightnessValue = 0,       \
};
#endif //PWM_TYPE == DIMMABLE_WHITE

#define COLOR_CONVERSION_DEBUG_PRINTS       ENABLED

#if (COLOR_CONVERSION_DEBUG_PRINTS == ENABLED)

#endif //COLOR_CONVERSION_DEBUG_PRINTS == ENABLED


#endif /* SRC_COLORCONVERSION_COLORCONVERSION_DEFINITIONS_H_ */
