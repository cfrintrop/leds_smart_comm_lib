#ifdef UNIT_TESTING
#include <gtest/gtest.h>


///Test main. Using the filter will only run certain tests to reduce console cluttering.

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    //::testing::FLAGS_gtest_filter = "test_Filter*";
    return RUN_ALL_TESTS();
}

TEST(test_Dummy, test_Dummy) {
    //Just a dummy test for now
    ASSERT_TRUE(true);
}

#endif
