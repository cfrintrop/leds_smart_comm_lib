#ifndef SRC_COMMLIB_INTERFACE_H_
#define SRC_COMMLIB_INTERFACE_H_

#include "CommLib_Definitions.h"
#include "ColorConversion.h"

//Initialisation
CommLib_errorCode_e CommLib_init(CommLib_callbacks_t *pCallbacks);

//Runtime
CommLib_errorCode_e CommLib_setLightOnOff(bool lightOn);
CommLib_errorCode_e CommLib_setLightValue_Alexa(CommLib_AlexaLightValues_t lightValue);
CommLib_errorCode_e CommLib_setLightValue_Homekit(CommLib_HomekitLightValues_t lightValue);

#if (NETWORK_DALI_SUPPORT == ENABLED)
CommLib_errorCode_e CommLib_onDaliForwardFrameReceived(uint8_t *pData, uint8_t length);
#endif //NETWORK_DALI_SUPPORT == ENABLED

#endif /* SRC_COMMLIB_INTERFACE_H_ */
