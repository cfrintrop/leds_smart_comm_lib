#ifndef SRC_COMMLIB_DEFINITIONS_H_
#define SRC_COMMLIB_DEFINITIONS_H_

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#include "CommLib_Config.h"

/******************************************GENERAL DEFINES******************************************/
#define DISABLED    0
#define ENABLED     1

#define DIMMABLE_WHITE              1
#define DIMMABLE_RGB                2
#define ADJUSTABLE_WHITE            3

/******************************************PREPROCESSOR ERRORS******************************************/
#if (PWM_TYPE == ADJUSTABLE_WHITE)
#error "Adjustable white currently not supported"
#endif //PWM_TYPE == ADJUSTABLE_WHITE

/******************************************TYPE DEFINITIONS******************************************/
#if (NETWORK_DALI_SUPPORT == ENABLED)
typedef void (*CommLib_daliWriteCallback_t) (uint8_t backwardFrame);
#endif //NETWORK_DALI_SUPPORT == ENABLED



typedef struct {
    bool isOn;
#if (PWM_TYPE == DIMMABLE_RGB)
    uint8_t hue;
    uint8_t saturation;
    uint8_t value;
#endif //PWM_TYPE == DIMMABLE_RGB

#if (PWM_TYPE == DIMMABLE_WHITE)
    uint8_t brightnessValue;    //TODO check type
#endif //PWM_TYPE == DIMMABLE_WHITE
} CommLib_AlexaLightValues_t;

typedef struct {
    bool isOn;
#if (PWM_TYPE == DIMMABLE_RGB)
    float hue;
    float saturation;
    int32_t brightness;
#endif //PWM_TYPE == DIMMABLE_RGB

#if (PWM_TYPE == DIMMABLE_WHITE)
    uint8_t brightnessValue;        //TODO check type
#endif //PWM_TYPE == DIMMABLE_WHITE
} CommLib_HomekitLightValues_t;

typedef enum {
    CommLib_errorCode_success               = 0x00,
    CommLib_errorCode_missingCallback       = 0x01,
    CommLib_errorCode_wrongDaliFrameLength  = 0x02,
    CommLib_errorCode_generalError          = 0xFF
} CommLib_errorCode_e;

typedef struct {
#if (PWM_TYPE == DIMMABLE_RGB)
    uint8_t redValue;
    uint8_t greenValue;
    uint8_t blueValue;
#endif //PWM_TYPE == DIMMABLE_RGB

#if (PWM_TYPE == DIMMABLE_WHITE)
    uint8_t brightnessValue;
#endif //PWM_TYPE == DIMMABLE_WHITE
} CommLib_pwmValue_t;

#if (PWM_TYPE == DIMMABLE_RGB)
#define DEFAULT_PWM_VALUE   \
{                           \
    .redValue = 0,          \
    .greenValue = 0,        \
    .blueValue = 0,         \
};

#endif //PWM_TYPE == DIMMABLE_RGB
#if (PWM_TYPE == DIMMABLE_WHITE)
#define DEFAULT_PWM_VALUE   \
{                           \
    .brightnessValue = 0,   \
};
#endif //PWM_TYPE == DIMMABLE_WHITE


typedef void (*CommLib_pwmChangeColorCallback_t) (CommLib_pwmValue_t);
typedef void (*CommLib_pwmTurnOnCallback_t) (void);
typedef void (*CommLib_pwmTurnOffCallback_t) (void);

typedef void (*CommLib_updateAlexaCallback_t) (CommLib_AlexaLightValues_t);
typedef void (*CommLib_updateHomekitCallback_t) (CommLib_HomekitLightValues_t);

typedef struct {
    CommLib_pwmChangeColorCallback_t pwmChangeColorCallback;
    CommLib_pwmTurnOnCallback_t pwmTurnOnCallback;
    CommLib_pwmTurnOffCallback_t pwmTurnOffCallback;

    CommLib_updateAlexaCallback_t updateAlexaCallback;
    CommLib_updateHomekitCallback_t updateHomekitCallback;

#if (NETWORK_DALI_SUPPORT == ENABLED)
    CommLib_daliWriteCallback_t daliWriteCallback;
#endif //NETWORK_DALI_SUPPORT == ENABLED
} CommLib_callbacks_t;

#endif /* SRC_COMMLIB_DEFINITIONS_H_ */
