#include "CommLib_Interface.h"
#include "CommLib.h"
#include "DaliCommands.h"

//Initialisation
CommLib_errorCode_e CommLib_init(CommLib_callbacks_t *pCallbacks) {
    return CommLib_setCallbacks(pCallbacks);
}

//Runtime
CommLib_errorCode_e CommLib_setLightOnOff(bool lightOn) {

    CommLib_setLightStatus(lightOn);
    if (lightOn) {
        return CommLib_executePWMTurnOnCallback();
    }
    else {
        return CommLib_executePWMTurnOffCallback();
    }

    return CommLib_errorCode_success;
}
CommLib_errorCode_e CommLib_setLightValue_Alexa(CommLib_AlexaLightValues_t alexaLightValue) {
    //TODO return errors from function calls
    CommLib_pwmValue_t pwmValue = ColorConversion_AlexaToPWM(alexaLightValue);
    CommLib_HomekitLightValues_t homekitLightValue = ColorConversion_AlexaToHomekit(alexaLightValue);

    CommLib_setCurrentPWMValue(pwmValue);
    CommLib_executePWMChangeColorCallback(pwmValue);

    CommLib_executeUpdateAlexaCallback(alexaLightValue);
    CommLib_executeUpdateHomekitCallback(homekitLightValue);

    return CommLib_errorCode_success;
}
CommLib_errorCode_e CommLib_setLightValue_Homekit(CommLib_HomekitLightValues_t homekitLightValue) {
    //TODO return errors from function calls
    CommLib_pwmValue_t pwmValue = ColorConversion_HomekitToPWM(homekitLightValue);
    CommLib_AlexaLightValues_t alexaLightValue = ColorConversion_HomekitToAlexa(homekitLightValue);

    CommLib_setCurrentPWMValue(pwmValue);
    CommLib_executePWMChangeColorCallback(pwmValue);

    CommLib_executeUpdateAlexaCallback(alexaLightValue);
    CommLib_executeUpdateHomekitCallback(homekitLightValue);
    return CommLib_errorCode_success;
}


#if (NETWORK_DALI_SUPPORT == ENABLED)
CommLib_errorCode_e CommLib_onDaliForwardFrameReceived(uint8_t *pData, uint8_t length) {
    //TODO Dali
}
#endif //NETWORK_DALI_SUPPORT == ENABLED

