
#ifndef SRC_DALI_DALIINTERNALSTORAGE_H_
#define SRC_DALI_DALIINTERNALSTORAGE_H_

#include "DaliDefinitions.h"

#if (NETWORK_DALI_SUPPORT == ENABLED)

DaliInternalStorage_Variables_t DaliInternalStorage_variables;
void DaliInternalStorage_reset(void);

#endif //NETWORK_DALI_SUPPORT == ENABLED

#endif /* SRC_DALI_DALIINTERNALSTORAGE_H_ */
