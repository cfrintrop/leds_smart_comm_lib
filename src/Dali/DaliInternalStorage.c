#include "DaliInternalStorage.h"

#if (NETWORK_DALI_SUPPORT == ENABLED)

DaliInternalStorage_Variables_t DaliInternalStorage_variables = {

    .actualLevel                    = 0x00,
    .targetLevel                    = 0x00,
    .lastActiveLevel                = 0x00,
    .lastLightLevel                 = DALI_DEFAULT_LAST_LIGHT_LEVEL,
    .powerOnLevel                   = DALI_DEFAULT_POWER_ON_LEVEL,
    .systemFailureLevel             = DALI_DEFAULT_SYSTEM_FAILURE_LEVEL,
    .minLevel                       = DALI_DEFAULT_MIN_LEVEL,
    .maxLevel                       = DALI_DEFAULT_MAX_LEVEL,
    .fadeRate                       = DALI_DEFAULT_FADE_RATE,
    .fadeTime                       = DALI_DEFAULT_FADE_TIME,
    .extendedFadeTimeBase           = DALI_DEFAULT_EXTENDED_FADE_TIME_BASE,
    .extendedFadeTimeMultiplier     = DALI_DEFAULT_EXTENDED_FADE_TIME_MULTIPLIER,
    .shortAddress                   = DALI_DEFAULT_SHORT_ADDRESS,
    .searchAddressHighBit           = 0x00,
    .searchAddressMidBit            = 0x00,
    .searchAddressLowBit            = 0x00,
    .randomAddressHighBit           = DALI_DEFAULT_RANDOM_ADDRESS_H,
    .randomAddressMidBit            = DALI_DEFAULT_RANDOM_ADDRESS_M,
    .randomAddressLowBit            = DALI_DEFAULT_RANDOM_ADDRESS_L,
    .operatingMode                  = DALI_OPERATING_MODE,
    .initialisationState            = 0x00,
    .writeEnabledState              = 0x00,
    .controlGearFailure             = 0x00,
    .lampFailure                    = 0x00,
    .lampOn                         = 0x00,
    .limitError                     = 0x00,
    .fadeRunning                    = 0x00,
    .resetState                     = DALI_DEFAULT_RESET_STATE,
    .powerCycleSeen                 = 0x00,
    .gearGroups                     = DALI_DEFAULT_GEAR_GROUPS,
    .sceneX                         = { DALI_DEFAULT_SCENE_X },
    .dtr0                           = 0x00,
    .dtr1                           = 0x01,
    .dtr2                           = 0x02,
    .phm                            = DALI_PHYSICAL_MINIMUM,
    .versionNumber                  = DALI_VERSION_NUMBER,
};

void DaliInternalStorage_reset(void) {
    DaliInternalStorage_variables.actualLevel                    = DALI_RESET_ACTUAL_LEVEL;
    DaliInternalStorage_variables.targetLevel                    = DALI_RESET_TARGET_LEVEL;
    DaliInternalStorage_variables.lastActiveLevel                = DALI_RESET_LAST_ACTIVE_LEVEL;
    DaliInternalStorage_variables.lastLightLevel                 = DALI_RESET_LAST_LIGHT_LEVEL;
    DaliInternalStorage_variables.powerOnLevel                   = DALI_RESET_POWER_ON_LEVEL;
    DaliInternalStorage_variables.systemFailureLevel             = DALI_RESET_SYSTEM_FAILURE_LEVEL;
    DaliInternalStorage_variables.minLevel                       = DALI_RESET_MIN_LEVEL;
    DaliInternalStorage_variables.maxLevel                       = DALI_RESET_MAX_LEVEL;
    DaliInternalStorage_variables.fadeRate                       = DALI_RESET_FADE_RATE;
    DaliInternalStorage_variables.fadeTime                       = DALI_RESET_FADE_TIME;
    DaliInternalStorage_variables.extendedFadeTimeBase           = DALI_RESET_EXTENDED_FADE_TIME_BASE;
    DaliInternalStorage_variables.extendedFadeTimeMultiplier     = DALI_RESET_EXTENDED_FADE_TIME_MULTIPLIER;
    DaliInternalStorage_variables.searchAddressHighBit           = DALI_RESET_SEARCH_ADDRESS_H;
    DaliInternalStorage_variables.searchAddressMidBit            = DALI_RESET_SEARCH_ADDRESS_M;
    DaliInternalStorage_variables.searchAddressLowBit            = DALI_RESET_SEARCH_ADDRESS_L;
    DaliInternalStorage_variables.randomAddressHighBit           = DALI_RESET_RANDOM_ADDRESS_H;
    DaliInternalStorage_variables.randomAddressMidBit            = DALI_RESET_RANDOM_ADDRESS_M;
    DaliInternalStorage_variables.randomAddressLowBit            = DALI_RESET_RANDOM_ADDRESS_L;
    DaliInternalStorage_variables.writeEnabledState              = DALI_RESET_WRITE_ENABLED_STATE;
    DaliInternalStorage_variables.controlGearFailure             = DALI_RESET_CONTROL_GEAR_FAILURE;
    DaliInternalStorage_variables.lampFailure                    = DALI_RESET_LAMP_FAILURE;
    DaliInternalStorage_variables.lampOn                         = DALI_RESET_LAMP_ON;
    DaliInternalStorage_variables.limitError                     = DALI_RESET_LIMIT_ERROR;
    DaliInternalStorage_variables.fadeRunning                    = DALI_RESET_FADE_RUNNING;
    DaliInternalStorage_variables.resetState                     = DALI_RESET_RESET_STATE;
    DaliInternalStorage_variables.powerCycleSeen                 = DALI_RESET_POWER_CYCLE_SEEN;
    DaliInternalStorage_variables.gearGroups                     = DALI_RESET_GEAR_GROUPS;
    DaliInternalStorage_variables.sceneX[0]                      = DALI_RESET_SCENE_X;
    DaliInternalStorage_variables.sceneX[1]                      = DALI_RESET_SCENE_X;
    DaliInternalStorage_variables.sceneX[2]                      = DALI_RESET_SCENE_X;
    DaliInternalStorage_variables.sceneX[3]                      = DALI_RESET_SCENE_X;
    DaliInternalStorage_variables.sceneX[4]                      = DALI_RESET_SCENE_X;
    DaliInternalStorage_variables.sceneX[5]                      = DALI_RESET_SCENE_X;
    DaliInternalStorage_variables.sceneX[6]                      = DALI_RESET_SCENE_X;
    DaliInternalStorage_variables.sceneX[7]                      = DALI_RESET_SCENE_X;
    DaliInternalStorage_variables.sceneX[8]                      = DALI_RESET_SCENE_X;
    DaliInternalStorage_variables.sceneX[9]                      = DALI_RESET_SCENE_X;
    DaliInternalStorage_variables.sceneX[10]                     = DALI_RESET_SCENE_X;
    DaliInternalStorage_variables.sceneX[11]                     = DALI_RESET_SCENE_X;
    DaliInternalStorage_variables.sceneX[12]                     = DALI_RESET_SCENE_X;
    DaliInternalStorage_variables.sceneX[13]                     = DALI_RESET_SCENE_X;
    DaliInternalStorage_variables.sceneX[14]                     = DALI_RESET_SCENE_X;
    DaliInternalStorage_variables.sceneX[15]                     = DALI_RESET_SCENE_X;
}

#endif //NETWORK_DALI_SUPPORT == ENABLED

