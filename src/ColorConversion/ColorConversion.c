#include "ColorConversion.h"

CommLib_pwmValue_t ColorConversion_AlexaToPWM(CommLib_AlexaLightValues_t input) {
    CommLib_pwmValue_t output = INITIAL_PWM_VALUE;
    //TODO

#if (COLOR_CONVERSION_DEBUG_PRINTS == ENABLED)
    printf("ColorConversion_AlexaToPWM\n");
    printf("Input = ");
    ColorConversion_printAlexaLightValues(input);
    printf("Output = ");
    ColorConversion_printPWMValues(output);
#endif //COLOR_CONVERSION_DEBUG_PRINTS == ENABLED
    return output;
}
CommLib_AlexaLightValues_t ColorConversion_PWMToAlexa(CommLib_pwmValue_t input) {
    CommLib_AlexaLightValues_t output = INITIAL_ALEXA_VALUE;
    //TODO

#if (COLOR_CONVERSION_DEBUG_PRINTS == ENABLED)
    printf("ColorConversion_AlexaToPWM\n");
    printf("Input = ");
    ColorConversion_printPWMValues(input);
    printf("Output = ");
    ColorConversion_printAlexaLightValues(output);
#endif //COLOR_CONVERSION_DEBUG_PRINTS == ENABLED
    return output;
}

CommLib_pwmValue_t ColorConversion_HomekitToPWM(CommLib_HomekitLightValues_t input) {
    CommLib_pwmValue_t output = INITIAL_PWM_VALUE;
    //TODO

#if (COLOR_CONVERSION_DEBUG_PRINTS == ENABLED)
    printf("ColorConversion_AlexaToPWM\n");
    printf("Input = ");
    ColorConversion_printHomekitLightValues(input);
    printf("Output = ");
    ColorConversion_printPWMValues(output);
#endif //COLOR_CONVERSION_DEBUG_PRINTS == ENABLED
    return output;
}
CommLib_HomekitLightValues_t ColorConversion_PWMToHomekit(CommLib_pwmValue_t input) {
    CommLib_HomekitLightValues_t output = INITIAL_HOMEKIT_VALUE;
    //TODO

#if (COLOR_CONVERSION_DEBUG_PRINTS == ENABLED)
    printf("ColorConversion_AlexaToPWM\n");
    printf("Input = ");
    ColorConversion_printPWMValues(input);
    printf("Output = ");
    ColorConversion_printHomekitLightValues(output);
#endif //COLOR_CONVERSION_DEBUG_PRINTS == ENABLED
    return output;
}

CommLib_HomekitLightValues_t ColorConversion_AlexaToHomekit(CommLib_AlexaLightValues_t input) {
    CommLib_HomekitLightValues_t output = INITIAL_HOMEKIT_VALUE;
    //TODO

#if (COLOR_CONVERSION_DEBUG_PRINTS == ENABLED)
    printf("ColorConversion_AlexaToPWM\n");
    printf("Input = ");
    ColorConversion_printAlexaLightValues(input);
    printf("Output = ");
    ColorConversion_printHomekitLightValues(output);
#endif //COLOR_CONVERSION_DEBUG_PRINTS == ENABLED
    return output;
}
CommLib_AlexaLightValues_t ColorConversion_HomekitToAlexa(CommLib_HomekitLightValues_t input) {
    CommLib_AlexaLightValues_t output = INITIAL_ALEXA_VALUE;
    //TODO

#if (COLOR_CONVERSION_DEBUG_PRINTS == ENABLED)
    printf("ColorConversion_AlexaToPWM\n");
    printf("Input = ");
    ColorConversion_printHomekitLightValues(input);
    printf("Output = ");
    ColorConversion_printAlexaLightValues(output);
#endif //COLOR_CONVERSION_DEBUG_PRINTS == ENABLED
    return output;
}

#if (COLOR_CONVERSION_DEBUG_PRINTS == ENABLED)
void ColorConversion_printAlexaLightValues(CommLib_AlexaLightValues_t alexaLightValues) {
#if (PWM_TYPE == DIMMABLE_RGB)
    printf("Hue: %d, Sat: %d, Val: %d\n", alexaLightValues.hue, alexaLightValues.saturation, alexaLightValues.value);
#endif //PWM_TYPE == DIMMABLE_RGB
#if (PWM_TYPE == DIMMABLE_WHITE)
    printf("Brightness: %d\n", alexaLightValues.brightnessValue);
#endif //PWM_TYPE == DIMMABLE_WHITE
}

void ColorConversion_printHomekitLightValues(CommLib_HomekitLightValues_t homekitLightValues) {
#if (PWM_TYPE == DIMMABLE_RGB)
    printf("Hue: %f, Sat: %f, Bri: %d\n", homekitLightValues.hue, homekitLightValues.saturation, homekitLightValues.brightness);
#endif //PWM_TYPE == DIMMABLE_RGB
#if (PWM_TYPE == DIMMABLE_WHITE)
    printf("Brightness: %d\n", homekitLightValues.brightnessValue);
#endif //PWM_TYPE == DIMMABLE_WHITE
}

void ColorConversion_printPWMValues(CommLib_pwmValue_t pwmLightValues) {
#if (PWM_TYPE == DIMMABLE_RGB)
    printf("Red: %d, Green: %d, Blue: %d\n", pwmLightValues.redValue, pwmLightValues.blueValue, pwmLightValues.greenValue);
#endif //PWM_TYPE == DIMMABLE_RGB
#if (PWM_TYPE == DIMMABLE_WHITE)
    printf("Brightness: %d\n", pwmLightValues.brightnessValue);
#endif //PWM_TYPE == DIMMABLE_WHITE
}
#endif //COLOR_CONVERSION_DEBUG_PRINTS == ENABLED
