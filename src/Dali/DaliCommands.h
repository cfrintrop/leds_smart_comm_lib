#ifndef SRC_DALI_DALICOMMANDS_H_
#define SRC_DALI_DALICOMMANDS_H_

#include "CommLib.h"
#include "DaliInternalStorage.h"
#include "DaliDefinitions.h"

#if (NETWORK_DALI_SUPPORT == ENABLED)

CommLib_errorCode_e DaliCommands_onCommandReceived(uint8_t address, uint8_t data);

extern DaliInternalStorage_Variables_t DaliInternalStorage_variables;

#endif //NETWORK_DALI_SUPPORT == ENABLED

#endif /* SRC_DALI_DALICOMMANDS_H_ */

