
#ifndef SRC_DALI_DALIDEFINITIONS_H_
#define SRC_DALI_DALIDEFINITIONS_H_

#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#define LENGTH_OF_DALI_FORWARD_FRAME 2

#define DALI_ADDRESS_TYPE_BITMASK                   (0b10000000)
#define DALI_ADDRESS_BITMASK                        (0b01111110)
#define DALI_ADDRESS_SIGNIFICANT_BIT_BITMASK        (0b00000001)

#define DALI_DIRECT_COMMAND_DIM_TO_OFF  0x00
#define DALI_DIRECT_COMMAND_MASK        0xFF

typedef void (*DaliCommands_CommandPtr_t) (uint8_t);

typedef struct {
    uint8_t highBit;
    uint8_t midBit;
    uint8_t lowBit;
} Dali_LongAddress_t;

#define DALI_PHYSICAL_MINIMUM       0x10    //TODO Dali
#define DALI_OPERATING_MODE         0       //TODO Dali
#define DALI_VERSION_NUMBER         1       //TODO Dali

#define DALI_MASK           0xFF
#define DALI_HALFMASK       0b00001111

#define DALI_DISABLED       0
#define DALI_ENABLED        1
#define DALI_WITHDRAWN      2

#define DALI_FALSE          0
#define DALI_TRUE           1

    /* Default (factory) values */
#define DALI_DEFAULT_LAST_LIGHT_LEVEL                   0xFE
#define DALI_DEFAULT_POWER_ON_LEVEL                     0xFE
#define DALI_DEFAULT_SYSTEM_FAILURE_LEVEL               0xFE
#define DALI_DEFAULT_MIN_LEVEL                          DALI_PHYSICAL_MINIMUM
#define DALI_DEFAULT_MAX_LEVEL                          0xFE
#define DALI_DEFAULT_FADE_RATE                          7
#define DALI_DEFAULT_FADE_TIME                          0
#define DALI_DEFAULT_EXTENDED_FADE_TIME_BASE            0
#define DALI_DEFAULT_EXTENDED_FADE_TIME_MULTIPLIER      0
#define DALI_DEFAULT_SHORT_ADDRESS                      DALI_MASK
#define DALI_DEFAULT_RANDOM_ADDRESS_H                   0xFF
#define DALI_DEFAULT_RANDOM_ADDRESS_M                   0xFF
#define DALI_DEFAULT_RANDOM_ADDRESS_L                   0xFF
#define DALI_DEFAULT_RESET_STATE                        DALI_TRUE
#define DALI_DEFAULT_GEAR_GROUPS                        0x0000
#define DALI_DEFAULT_SCENE_X                            DALI_MASK

    /* Reset values */
#define DALI_RESET_ACTUAL_LEVEL                         0xFE
#define DALI_RESET_TARGET_LEVEL                         0xFE
#define DALI_RESET_LAST_ACTIVE_LEVEL                    0xFE
#define DALI_RESET_LAST_LIGHT_LEVEL                     0xFE
#define DALI_RESET_POWER_ON_LEVEL                       0xFE
#define DALI_RESET_SYSTEM_FAILURE_LEVEL                 0xFE
#define DALI_RESET_MIN_LEVEL                            DALI_PHYSICAL_MINIMUM
#define DALI_RESET_MAX_LEVEL                            0xFE
#define DALI_RESET_FADE_RATE                            7
#define DALI_RESET_FADE_TIME                            0
#define DALI_RESET_EXTENDED_FADE_TIME_BASE              0
#define DALI_RESET_EXTENDED_FADE_TIME_MULTIPLIER        0
#define DALI_RESET_SEARCH_ADDRESS_H                     0xFF
#define DALI_RESET_SEARCH_ADDRESS_M                     0xFF
#define DALI_RESET_SEARCH_ADDRESS_L                     0xFF
#define DALI_RESET_RANDOM_ADDRESS_H                     0xFF
#define DALI_RESET_RANDOM_ADDRESS_M                     0xFF
#define DALI_RESET_RANDOM_ADDRESS_L                     0xFF
#define DALI_RESET_WRITE_ENABLED_STATE                  DALI_DISABLED
#define DALI_RESET_CONTROL_GEAR_FAILURE                 DALI_FALSE
#define DALI_RESET_LAMP_FAILURE                         DALI_FALSE
#define DALI_RESET_LAMP_ON                              DALI_FALSE
#define DALI_RESET_LIMIT_ERROR                          DALI_FALSE
#define DALI_RESET_FADE_RUNNING                         DALI_FALSE
#define DALI_RESET_RESET_STATE                          DALI_TRUE
#define DALI_RESET_POWER_CYCLE_SEEN                     DALI_FALSE
#define DALI_RESET_GEAR_GROUPS                          0x0000
#define DALI_RESET_SCENE_X                              DALI_MASK

    /* Power On value */
#define DALI_POWER_ON_ACTUAL_LEVEL                      0x00
#define DALI_POWER_ON_TARGET_LEVEL                      0x00
#define DALI_POWER_ON_LAST_ACTIVE_LEVEL                 MAX_LEVEL //TODO Dali
#define DALI_POWER_ON_SEARCH_ADDRESS_H                  0xFF
#define DALI_POWER_ON_SEARCH_ADDRESS_M                  0xFF
#define DALI_POWER_ON_SEARCH_ADDRESS_L                  0xFF
#define DALI_POWER_ON_INITIALISATION_STATE              DALI_DISABLED
#define DALI_POWER_ON_WRITE_ENABLED_STATE               DALI_DISABLED
#define DALI_POWER_ON_CONTROL_GEAR_FAILURE              DALI_FALSE
#define DALI_POWER_ON_LAMP_FAILURE                      DALI_FALSE
#define DALI_POWER_ON_LAMP_ON                           DALI_FALSE
#define DALI_POWER_ON_LIMIT_ERROR                       DALI_FALSE
#define DALI_POWER_ON_FADE_RUNNING                      DALI_FALSE
#define DALI_POWER_ON_RESET_STATE                       DALI_TRUE
#define DALI_POWER_ON_POWER_CYCLE_SEEN                  DALI_TRUE

#define DALI_NUMBER_OF_SCENES                           16
#define DALI_NUMBER_OF_GROUPS                           16

    /* Dali Variables */
typedef struct {
    uint8_t actualLevel;
    uint8_t targetLevel;
    uint8_t lastActiveLevel;
    uint8_t lastLightLevel;
    uint8_t powerOnLevel;
    uint8_t systemFailureLevel;
    uint8_t minLevel;
    uint8_t maxLevel;
    uint8_t fadeRate;
    uint8_t fadeTime;
    uint8_t extendedFadeTimeBase;
    uint8_t extendedFadeTimeMultiplier;
    uint8_t shortAddress;
    uint8_t searchAddressHighBit;
    uint8_t searchAddressMidBit;
    uint8_t searchAddressLowBit;
    uint8_t randomAddressHighBit;
    uint8_t randomAddressMidBit;
    uint8_t randomAddressLowBit;
    uint8_t operatingMode;
    uint8_t initialisationState;
    uint8_t writeEnabledState;
    uint8_t controlGearFailure;
    uint8_t lampFailure;
    uint8_t lampOn;
    uint8_t limitError;
    uint8_t fadeRunning;
    uint8_t resetState;
    uint8_t powerCycleSeen;
    uint16_t gearGroups;
    uint8_t sceneX[DALI_NUMBER_OF_SCENES];
    uint8_t dtr0;
    uint8_t dtr1;
    uint8_t dtr2;
    uint8_t phm;
    uint8_t versionNumber;
} DaliInternalStorage_Variables_t;


#endif /* SRC_DALI_DALIDEFINITIONS_H_ */
