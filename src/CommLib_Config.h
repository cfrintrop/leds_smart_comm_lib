#ifndef SRC_COMMLIB_CONFIG_H_
#define SRC_COMMLIB_CONFIG_H_

#include "CommLib_Definitions.h"
/******************************************DEVICE FEATURES******************************************/
#define PWM_TYPE                    DIMMABLE_RGB

#define NETWORK_DALI_SUPPORT        DISABLED

#endif /* SRC_COMMLIB_CONFIG_H_ */
